# PTExam API 1.0.2.RELEASE

## Futtatási követelmények

- Java 8
- Maven

Ebben az olvass el fájlban található összes parancs Maven 3.6.1.-el le lett tesztelve.

## Projekt frissítése

Mivel a master ágba fut be az összes merge (release), ezért a master ágon maradva elég a következő parancsot kiadni:

```
git pull
```

## Tesztelés

Mivel a projekt alap profil beállítása a "dev" és a "test", ezért elég a következő parancsot kiadni:

```
mvn clean test
```

vagy ha szeretnénk biztosra menni, hogy a test profillal fut le a teszt, akkor ezt a következő módon tehetjük meg:

```
mvn clean test -D spring.profiles.active=test
```

## Telepítés és/vagy futtatás

### Módszer: 1

Ha külön szeretnénk telepíteni és futtatni az egész projektet, akkor a következő két parancsot kell lefuttatnunk:

```
mvn clean install
java -jar target/ptexam-api.jar
```

Mivel, ahogy azt már említettem, "dev" és "test" profillal indul az alkalmazás, de ha biztosra szeretnénk menni, hogy a "dev" profil lesz aktív, akkor a következő módon érhetjük el:

```
java -jar target/ptexam-api.jar --spring.profiles.active=dev
```

### Módszer: 2

Ha nem szeretnénk külön telepíteni és futtatni, akkor ezt megtehetjük egy paranccsal is, ami a következő:

```
mvn clean spring-boot:run
```

Profilt aktiválni az alábbi módon lehet:

```
mvn clean spring-boot:run -D spring.profiles.active=dev
```

### Futó rendszer használata:

Miután sikeresen elindult az API, a következő URL-en lehet kezdeni a tevékenységeinket:

```
http://localhost:8082/
```

Felhasználó azonosításra a következő ( **név ; jelszó** ) kombinációk használhatók:

- **SysAdmin ; n-8B7+6a** (Tanár, Diák és Admin jogok)
- **teacher ; teacher** (Tanár jogok)
- **student ; student** (Diák jogok)

A kitölthető feladatlapokat a következő URL-en érhetjük el (diákként):

```
http://localhost:8082/api/v1/tasksheets/search/findAllAvailable
```

Feladat választ POST-olni a következő URL-en lehet:

```
http://localhost:8082/api/v1/tasksheets/{vizsga-azonosító}/submit/{feladatlap-feladat-azonosító}
```

Feladatlapot beadni a következő URL-en lehet:

```
http://localhost:8082/api/v1/tasksheets/{vizsga-azonosító}/terminate
```

További használata a rendszernek a docs/documentation.html fájlban olvasható.