# Changelog

## [1.0.0.1]
### Added
- New subject functions.
- Switched to HTTPS protocol.
- Application is redirecting requests from HTTP (port: 8080) to HTTPS (port: 8443).
- Added two profiles to project, one with H2 database (PROFILE_DEV: dev) and one with PostgreSQL database (PROFILE_MASTER: master).

## [1.0.0.2]
### Added
- New topic functions.
- New task functions.
- New solution functions.
- New classroom functions.
- New exam functions.
- Added dummy data loader for dev profile (H2).

## [1.0.0.3]
### Added
- New task sheet generation function.
- New task sheet answer submit function.
- Enabled webAllowOthers in development mode for H2 database.
- Custom IPV4 support, with auto converter.
- IPV4 auditing support for domain and abstract domain.
- Switched to HTTP protocol and switched port to 8082.
- New 'nice' projections.
### Changed
- HTTPS protocol, will use a proxy in docker to handle secured connections. 

## [1.0.0.4]
### Added
- Custom Basic Authentication with user, role, privilege support.
- Enabled CORS in the API.
- Added PrincipalUtils for easier access to current logged in user's name.
- Prepared project for Spring REST Docs.
### Changed
- Removed logger and it's remains completely from IPV4Utils.

## [1.0.0.5]
### Added
- New result functions.
- New user functions.
### Fixed
- Fixed code smells and minor bugs.
### Changed
- Project structure update.

## [1.0.1.1] - 2019-05-15
### Added
- Security Access update (Roles were used).
- Added JUnit 5 tests for the full API.
- Generated documentation from tests with Spring REST Docs.
- Reviewed whole project to apply a potential release candidate.
- New test profile.
### Fixed
- List/Collection major bug fixed with changing all to Set.
- Password violation.
- Major bug fix, missing eager fetch at user roles.
### Changed
- Updated CHANGELOG file structure with template to easier understandment.
- Optimized CORSConfig, H2DataLoader.