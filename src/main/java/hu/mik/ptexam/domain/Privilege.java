package hu.mik.ptexam.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "privileges")
@EqualsAndHashCode(callSuper = false, exclude = { "roles" })
@SequenceGenerator(
	name = Privilege.GENERATOR,
	sequenceName = Privilege.PRIVILEGE_ID_SEQ,
	initialValue = 1000,
	allocationSize = 1
)
public class Privilege extends Domain {
	
	private static final long serialVersionUID = 1L;

	public static final String PRIVILEGE_ID_SEQ = "privilege_id_seq";
	public static final String PRIVILEGE_NAME = "privilege_name";

	@Column(name = PRIVILEGE_NAME, nullable = false, unique = true)
	private String name;
	
	@JsonIgnore
	@Builder.Default
	@ManyToMany(mappedBy = "privileges")
	private Set<Role> roles = new HashSet<>();
	
}
