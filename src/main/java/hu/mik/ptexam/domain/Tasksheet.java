package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Tasksheet.TBL_TASKSHEETS)
@EqualsAndHashCode(callSuper = false, exclude = { "tasksheetTasks" })
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = Tasksheet.TASKSHEET_ID_SEQ, initialValue=1000, allocationSize = 1)
public class Tasksheet extends Domain {

	private static final long serialVersionUID = 1L;

	public static final String TASKSHEET_ID_SEQ = "tasksheet_id_seq";
	public static final String TBL_TASKSHEETS = "tasksheets";
	public static final String FLD_TASKSHEET = "tasksheet";
	public static final String FLD_TASKSHEETS = "tasksheets";
	public static final String COL_USERNAME = "tasksheet_username";
	public static final String COL_IS_TERMINATED = "tasksheet_is_terminated";
	public static final String COL_TASKSHEET_VERSION = "tasksheet_version";
	
	@ManyToOne
	private Exam exam;
	
	@JsonProperty(value = COL_IS_TERMINATED, access = READ_WRITE)
	@Column(name = COL_IS_TERMINATED, insertable = true, nullable = false, updatable = true, unique = false)
	private boolean terminated;
	
	@JsonProperty(value = COL_USERNAME, access = READ_WRITE)
	@Column(name = COL_USERNAME, insertable = true, nullable = false, updatable = true, unique = false)
	private String username;
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_TASKSHEET)
	private Set<TasksheetTask> tasksheetTasks = new HashSet<>();
	
	@JsonProperty(value = COL_TASKSHEET_VERSION, access = READ_ONLY)
	@Transient
	private Long tasksheetVersion;
	
	public Long getTasksheetVersion() {
		return getVersion();
	}
	
}
