package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Subject.TBL_SUBJECTS)
@EqualsAndHashCode(callSuper = false, exclude = { "topics" })
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = Subject.SUBJECT_ID_SEQ, initialValue=1000, allocationSize = 1)
public class Subject extends Domain {

	private static final long serialVersionUID = 1L;

	public static final String SUBJECT_ID_SEQ = "subject_id_seq";
	public static final String TBL_SUBJECTS = "subjects";
	public static final String FLD_SUBJECT = "subject";
	public static final String FLD_SUBJECTS = "subjects";
	public static final String COL_CODE = "subject_code";
	public static final String COL_NAME = "subject_name";
	public static final String COL_SUBJECT_VERSION = "subject_version";

	@JsonProperty(value = COL_CODE, access = READ_WRITE)
	@Column(name = COL_CODE, insertable = true, nullable = false, updatable = true, unique = true)
	private String code;
	
	@JsonProperty(value = COL_NAME, access = READ_WRITE)
	@Column(name = COL_NAME, insertable = true, nullable = false, updatable = true, unique = false)
	private String name;
	
	@JsonProperty(value = COL_SUBJECT_VERSION, access = READ_ONLY)
	@Transient
	private Long subjectVersion;
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_SUBJECT)
	private Set<Topic> topics = new HashSet<>();
	
	public Long getSubjectVersion() {
		return getVersion();
	}

}
