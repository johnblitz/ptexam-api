package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Topic.TBL_TOPICS)
@EqualsAndHashCode(callSuper = false, exclude = { "tasks", "exams" })
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = Topic.TOPIC_ID_SEQ, initialValue=1000, allocationSize = 1)
public class Topic extends Domain {

	private static final long serialVersionUID = 1L;

	public static final String TOPIC_ID_SEQ = "topic_id_seq";
	public static final String TBL_TOPICS = "topics";
	public static final String FLD_TOPIC = "topic";
	public static final String FLD_TOPICS = "topics";
	public static final String COL_NAME = "topic_name";
	public static final String COL_TOPIC_VERSION = "topic_version";
	
	@JsonProperty(value = COL_NAME, access = READ_WRITE)
	@Column(name = COL_NAME, insertable = true, nullable = false, updatable = true, unique = true)
	private String name;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Subject subject;
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_TOPIC)
	private Set<Task> tasks = new HashSet<>();
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_TOPIC)
	private Set<Exam> exams = new HashSet<>();
	
	@JsonProperty(value = COL_TOPIC_VERSION, access = READ_ONLY)
	@Transient
	private Long topicVersion;
	
	public Long getTopicVersion() {
		return getVersion();
	}
	
}
