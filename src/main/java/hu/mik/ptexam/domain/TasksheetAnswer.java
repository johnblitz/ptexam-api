package hu.mik.ptexam.domain;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TasksheetAnswer.TBL_TASKSHEET_ANSWERS)
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = TasksheetAnswer.TASKSHEET_ANSWER_ID_SEQ, initialValue = 1000,
		allocationSize = 1)
public class TasksheetAnswer extends Domain {

	private static final long serialVersionUID = 1L;
	
	public static final String TASKSHEET_ANSWER_ID_SEQ = "tasksheet_answers_id_seq";
	public static final String TBL_TASKSHEET_ANSWERS = "tasksheet_answers";
	public static final String FLD_TASKSHEET_ANSWER = "tasksheetAnswer";
	public static final String FLD_TASKSHEET_ANSWERS = "tasksheetAnswers";
	public static final String COL_COMMENT = "tasksheet_answer_comment";
	public static final String COL_USERNAME = "tasksheet_answer_username";
	public static final String COL_IS_LATE = "tasksheet_answer_is_late";
	public static final String COL_LATENESS_IN_SECONDS = "tasksheet_answer_lateness_in_seconds";
	public static final String COL_TASKSHEET_ANSWER_VERSION = "tasksheet_answer_version";
	
	@JsonIgnore
	@ManyToOne
	private TasksheetTask tasksheetTask;
	
	@JsonProperty(value = COL_COMMENT, access = READ_WRITE)
	@Column(name = COL_COMMENT, insertable = true, nullable = true, updatable = true, unique = false)
	private String comment;
	
	@JsonProperty(value = COL_USERNAME, access = READ_WRITE)
	@Column(name = COL_USERNAME, insertable = true, nullable = false, updatable = true, unique = false)
	private String username;
	
	@Builder.Default
	@JsonProperty(value = COL_IS_LATE, access = READ_WRITE)
	@Column(name = COL_IS_LATE, insertable = true, nullable = false, updatable = true, unique = false)
	private boolean late = false;
	
	@Builder.Default
	@JsonProperty(value = COL_LATENESS_IN_SECONDS, access = READ_WRITE)
	@Column(name = COL_LATENESS_IN_SECONDS, insertable = true, nullable = false, updatable = true, unique = false)
	private Long latenessInSeconds = 0L;
	
	@ManyToOne
	private Solution solution;
	
	@JsonProperty(value = COL_TASKSHEET_ANSWER_VERSION, access = READ_ONLY)
	@Transient
	private Long tasksheetAnswerVersion;
	
	public Long getTasksheetAnswerVersion() {
		return getVersion();
	}
	
}
