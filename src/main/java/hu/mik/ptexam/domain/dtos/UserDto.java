package hu.mik.ptexam.domain.dtos;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.mik.ptexam.domain.*;

@Getter @Setter
public class UserDto {

	@JsonProperty(value = User.COL_USER_NAME)
	private String username;
	@JsonProperty(value = User.COL_USER_PSSWRD)
	private String password;
	@JsonProperty(value = Role.ROLE_NAME)
	private String role;
	
}
