package hu.mik.ptexam.domain.dtos;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.mik.ptexam.domain.*;

@Getter @Setter
public class AnswerDto {

	@JsonProperty(value = TasksheetAnswer.COL_COMMENT)
	private String comment;
	@JsonProperty(value = Solution.FLD_SOLUTIONS)
	private List<String> solutions;
	
}
