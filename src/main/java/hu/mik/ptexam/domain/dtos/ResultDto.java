package hu.mik.ptexam.domain.dtos;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

import hu.mik.ptexam.domain.Result;

@Getter @Setter
public class ResultDto {

	@JsonProperty(value = Result.COL_GRADE)
	private Integer grade;
	@JsonProperty(value = Result.COL_POINTS)
	private Double points;
	
}
