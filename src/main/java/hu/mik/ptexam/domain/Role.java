package hu.mik.ptexam.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "roles")
@EqualsAndHashCode(callSuper = false, exclude = { "users" })
@SequenceGenerator(
	name = Role.GENERATOR,
	sequenceName = Role.ROLE_ID_SEQ,
	initialValue = 1000,
	allocationSize = 1
)
public class Role extends Domain {

	private static final long serialVersionUID = 1L;

	public static final String ROLE_ID_SEQ = "role_id_seq";
	public static final String ROLE_NAME = "role_name";
	
	@Column(name = ROLE_NAME, nullable = false, unique = true)
	private String name;
	
	@JsonIgnore
	@Builder.Default
	@ManyToMany(mappedBy = "roles")
	private Set<User> users = new HashSet<>();
	
	@Builder.Default
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "roles_privileges",
		joinColumns = @JoinColumn(
			name = "role_id",
			referencedColumnName = COL_ID
		),
		inverseJoinColumns = @JoinColumn(
			name = "privilege_id",
			referencedColumnName = COL_ID
		)
	)
	private Set<Privilege> privileges = new HashSet<>();
	
}
