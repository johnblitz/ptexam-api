package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Exam.TBL_EXAMS)
@EqualsAndHashCode(callSuper = false, exclude = { "tasksheets" })
@GenericGenerator(name = AbstractDomain.GENERATOR, strategy = AbstractDomain.HASH_STRATEGY)
public class Exam extends AbstractDomain<String> {

	private static final long serialVersionUID = 1L;

	public static final String TBL_EXAMS = "exams";
	public static final String FLD_EXAM = "exam";
	public static final String FLD_EXAMS = "exams";
	public static final String COL_DESCRIPTION = "exam_description";
	public static final String COL_START_TIME = "exam_start_time";
	public static final String COL_END_TIME = "exam_end_time";
	public static final String COL_TASK_COUNT = "exam_task_count";
	public static final String COL_EXAM_VERSION = "exam_version";
	
	@JsonProperty(value = COL_DESCRIPTION, access = READ_WRITE)
	@Column(name = COL_DESCRIPTION, insertable = true, nullable = true, updatable = true, unique = false)
	private String description;
	
	@JsonProperty(value = COL_START_TIME, access = READ_WRITE)
	@Column(name = COL_START_TIME, insertable = true, nullable = false, updatable = true, unique = false)
	private LocalDateTime startTime;
	
	@JsonProperty(value = COL_END_TIME, access = READ_WRITE)
	@Column(name = COL_END_TIME, insertable = true, nullable = false, updatable = true, unique = false)
	private LocalDateTime endTime;
	
	@JsonProperty(value = COL_TASK_COUNT, access = READ_WRITE)
	@Column(name = COL_TASK_COUNT, insertable = true, nullable = false, updatable = true, unique = false)
	private Integer taskCount;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Classroom classroom;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Topic topic;
	
	@JsonIgnore
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_EXAM)
	private Set<Tasksheet> tasksheets = new HashSet<>();
	
	@JsonProperty(value = COL_EXAM_VERSION, access = READ_ONLY)
	@Transient
	private Long examVersion;
	
	public Long getExamVersion() {
		return getVersion();
	}
	
}
