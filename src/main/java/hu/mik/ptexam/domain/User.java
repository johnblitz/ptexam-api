package hu.mik.ptexam.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@EqualsAndHashCode(callSuper = false, exclude = { "roles" })
@SequenceGenerator(
	name = User.GENERATOR,
	sequenceName = User.USER_ID_SEQ,
	initialValue = 1000,
	allocationSize = 1
)
public class User extends Domain {

	private static final long serialVersionUID = 1L;
	
	public static final String USER_ID_SEQ = "user_id_seq";
	public static final String COL_USER_NAME = "user_name";
	public static final String COL_USER_LASTNAME = "user_lastname";
	public static final String COL_USER_FIRSTNAME = "user_firstname";
	public static final String COL_USER_PSSWRD = "user_psswrd";
	public static final String COL_USER_EMAIL = "user_email";
	public static final String COL_USER_IS_ENABLED = "user_is_enabled";
	public static final String COL_USER_IS_ACCOUNT_NON_EXPIRED =
		"user_is_account_non_expired";
	public static final String COL_USER_IS_ACCOUNT_NON_LOCKED =
		"user_is_account_non_locked";
	public static final String COL_USER_IS_CREDENTIALS_NON_EXPIRED =
		"user_is_credentials_non_expired";

	@Column(name = COL_USER_NAME, nullable = false, unique = true)
	private String username;
	@Column(name = COL_USER_LASTNAME)
	private String lastName;
	@Column(name = COL_USER_FIRSTNAME)
	private String firstName;
	@Column(name = COL_USER_PSSWRD)
	private String password;
	@Column(name = COL_USER_EMAIL)
	private String email;
	@Column(name = COL_USER_IS_ENABLED)
	private boolean enabled;
	@Column(name = COL_USER_IS_ACCOUNT_NON_EXPIRED)
	private boolean accountNonExpired;
	@Column(name = COL_USER_IS_ACCOUNT_NON_LOCKED)
	private boolean accountNonLocked;
	@Column(name = COL_USER_IS_CREDENTIALS_NON_EXPIRED)
	private boolean credentialsNonExpired;
	
	@Builder.Default
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "users_roles",
		joinColumns = @JoinColumn(
			name = "user_id",
			referencedColumnName = COL_ID
		),
		inverseJoinColumns = @JoinColumn(
			name = "role_id",
			referencedColumnName = COL_ID
		)
	)
	private Set<Role> roles = new HashSet<>();
	
}
