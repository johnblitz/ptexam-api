package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = TasksheetTask.TBL_TASKSHEET_TASKS)
@EqualsAndHashCode(callSuper = false, exclude = { "tasksheetAnswers" })
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = TasksheetTask.TASKSHEET_TASK_ID_SEQ, initialValue = 1000,
		allocationSize = 1)
public class TasksheetTask extends Domain {

	private static final long serialVersionUID = 1L;

	public static final String TASKSHEET_TASK_ID_SEQ = "tasksheet_task_id_seq";
	public static final String TBL_TASKSHEET_TASKS = "tasksheet_tasks";
	public static final String FLD_TASKSHEET_TASK = "tasksheetTask";
	public static final String FLD_TASKSHEET_TASKS = "tasksheetTasks";
	public static final String COL_IS_ANSWERED = "tasksheet_task_is_answered";
	public static final String COL_TASKSHEET_TASK_VERSION = "tasksheet_task_version";
	
	@JsonIgnore
	@ManyToOne
	private Tasksheet tasksheet;
	
	@JsonProperty(value = COL_IS_ANSWERED, access = READ_WRITE)
	@Column(name = COL_IS_ANSWERED, insertable = true, nullable = false, updatable = true, unique = false)
	private boolean answered;
	
	@ManyToOne
	private Task task;
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_TASKSHEET_TASK, orphanRemoval = true)
	private Set<TasksheetAnswer> tasksheetAnswers = new HashSet<>();
	
	@JsonProperty(value = COL_TASKSHEET_TASK_VERSION, access = READ_ONLY)
	@Transient
	private Long tasksheetTaskVersion;
	
	public Long getTasksheetTaskVersion() {
		return getVersion();
	}
	
}
