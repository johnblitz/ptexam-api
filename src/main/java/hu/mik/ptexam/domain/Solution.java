package hu.mik.ptexam.domain;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Solution.TBL_SOLUTIONS)
@EqualsAndHashCode(callSuper = false)
@GenericGenerator(name = AbstractDomain.GENERATOR, strategy = AbstractDomain.HASH_STRATEGY)
public class Solution extends AbstractDomain<String> {

	private static final long serialVersionUID = 1L;

	public static final String TBL_SOLUTIONS = "solutions";
	public static final String FLD_SOLUTION = "solution";
	public static final String FLD_SOLUTIONS = "solutions";
	public static final String COL_ANSWER = "solution_answer";
	public static final String COL_IS_CORRECT = "solution_is_correct";
	public static final String COL_SOLUTION_VERSION = "solution_version";
	
	@JsonProperty(value = COL_ANSWER, access = READ_WRITE)
	@Column(name = COL_ANSWER, insertable = true, nullable = false, updatable = true, unique = false)
	private String answer;
	
	@JsonProperty(value = COL_IS_CORRECT, access = READ_WRITE)
	@Column(name = COL_IS_CORRECT, insertable = true, nullable = false, updatable = true, unique = false)
	private boolean correct;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Task task;
	
	@JsonProperty(value = COL_SOLUTION_VERSION, access = READ_ONLY)
	@Transient
	private Long solutionVersion;
	
	public Long getSolutionVersion() {
		return getVersion();
	}
	
}
