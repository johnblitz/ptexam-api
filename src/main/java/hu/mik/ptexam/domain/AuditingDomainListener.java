package hu.mik.ptexam.domain;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import hu.mik.ptexam.utils.IPV4;
import hu.mik.ptexam.utils.IPV4Utils;

public class AuditingDomainListener {

	@PrePersist
	private void onPrePersist(Domain domain) {
		IPV4 ip = IPV4Utils.getClientIP();
		domain.setCreatedIp(ip);
		domain.setLastModifiedIp(ip);
	} 
	
	@PreUpdate
	private void onPreUpdate(Domain domain) {
		IPV4 ip = IPV4Utils.getClientIP();
		domain.setLastModifiedIp(ip);
	}
	
}
