package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import hu.mik.ptexam.utils.IPV4;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Classroom.TBL_CLASSROOMS)
@EqualsAndHashCode(callSuper = false, exclude = { "exams" })
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = Classroom.CLASSROOM_ID_SEQ, initialValue=1000, allocationSize = 1)
public class Classroom extends Domain {

	private static final long serialVersionUID = 1L;

	public static final String CLASSROOM_ID_SEQ = "classroom_id_seq";
	public static final String TBL_CLASSROOMS = "classrooms";
	public static final String FLD_CLASSROOM = "classroom";
	public static final String FLD_CLASSROOMS = "classrooms";
	public static final String COL_LOCATION = "classroom_location";
	public static final String COL_IP_START = "classroom_ip_start";
	public static final String COL_IP_END = "classroom_ip_end";
	public static final String COL_CLASSROOM_VERSION = "classroom_version";

	@JsonProperty(value = COL_LOCATION, access = READ_WRITE)
	@Column(name = COL_LOCATION, insertable = true, nullable = false, updatable = true, unique = true)
	private String location;
	
	@JsonProperty(value = COL_IP_START, access = READ_WRITE)
	@Column(name = COL_IP_START, nullable = false, updatable = true)
	private IPV4 ipStart;
	
	@JsonProperty(value = COL_IP_END, access = READ_WRITE)
	@Column(name = COL_IP_END, nullable = false, updatable = true)
	private IPV4 ipEnd;
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_CLASSROOM)
	private Set<Exam> exams = new HashSet<>();
	
	@JsonProperty(value = COL_CLASSROOM_VERSION, access = READ_ONLY)
	@Transient
	private Long classroomVersion;
	
	public Long getClassroomVersion() {
		return getVersion();
	}
	
}
