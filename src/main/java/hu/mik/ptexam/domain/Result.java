package hu.mik.ptexam.domain;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.persistence.SequenceGenerator;
import com.fasterxml.jackson.annotation.JsonProperty;
import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Result.TBL_RESULTS)
@EqualsAndHashCode(callSuper = false, exclude = { "tasksheet" })
@SequenceGenerator(name = Domain.GENERATOR, sequenceName = Result.RESULT_ID_SEQ, initialValue=1000, allocationSize = 1)
public class Result extends Domain {

	private static final long serialVersionUID = 1L;
	
	public static final String RESULT_ID_SEQ = "result_id_seq";
	public static final String TBL_RESULTS = "results";
	public static final String FLD_RESULT = "result";
	public static final String FLD_RESULTS = "results";
	public static final String COL_USERNAME = "result_username";
	public static final String COL_GRADE = "result_grade";
	public static final String COL_POINTS = "result_points";
	public static final String COL_RESULT_VERSION = "result_version";
	
	@JsonProperty(value = COL_USERNAME, access = READ_WRITE)
	@Column(name = COL_USERNAME, insertable = true, nullable = false, updatable = true, unique = false)
	private String username;

	@OneToOne
	private Tasksheet tasksheet;

	@JsonProperty(value = COL_GRADE, access = READ_WRITE)
	@Column(name = COL_GRADE)
	private Integer grade;
	
	@JsonProperty(value = COL_POINTS, access = READ_WRITE)
	@Column(name = COL_POINTS)
	private Double points;
	
	@JsonProperty(value = COL_RESULT_VERSION, access = READ_ONLY)
	@Transient
	private Long resultVersion;
	
	
	public Long getResultVersion() {
		return getVersion();
	}
	
}
