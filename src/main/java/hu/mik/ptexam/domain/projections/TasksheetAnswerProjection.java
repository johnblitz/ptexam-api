package hu.mik.ptexam.domain.projections;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hu.mik.ptexam.domain.Solution;
import hu.mik.ptexam.domain.TasksheetAnswer;

@Projection(types = { TasksheetAnswer.class })
@JsonPropertyOrder(alphabetic = false, value = {
	TasksheetAnswer.COL_ID,
	TasksheetAnswer.COL_USERNAME,
	TasksheetAnswer.COL_IS_LATE,
	TasksheetAnswer.COL_LATENESS_IN_SECONDS,
	TasksheetAnswer.COL_TASKSHEET_ANSWER_VERSION,
	TasksheetAnswer.COL_COMMENT,
	Solution.FLD_SOLUTION
})
public interface TasksheetAnswerProjection {

	@JsonProperty(value = TasksheetAnswer.COL_ID)
	Long getId();
	
	@JsonProperty(value = TasksheetAnswer.COL_USERNAME)
	String getUsername();
	
	@JsonProperty(value = TasksheetAnswer.COL_COMMENT)
	String getComment();
	
	@JsonProperty(value = TasksheetAnswer.COL_IS_LATE)
	boolean isLate();
	
	@JsonProperty(value = TasksheetAnswer.COL_LATENESS_IN_SECONDS)
	Long getLatenessInSeconds();
	
	@JsonProperty(value = Solution.FLD_SOLUTION)
	SolutionProjection getSolution();
	
	@JsonProperty(value = TasksheetAnswer.COL_TASKSHEET_ANSWER_VERSION)
	Long getTasksheetAnswerVersion();
	
}
