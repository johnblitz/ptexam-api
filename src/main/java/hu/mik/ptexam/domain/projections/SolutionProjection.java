package hu.mik.ptexam.domain.projections;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hu.mik.ptexam.domain.Solution;

@Projection(types = { Solution.class })
@JsonPropertyOrder(alphabetic = false, value = {
	Solution.COL_ID,
	Solution.COL_SOLUTION_VERSION,
	Solution.COL_ANSWER
})
public interface SolutionProjection {
	
	@JsonProperty(value = Solution.COL_ID)
	String getId();
	
	@JsonProperty(value = Solution.COL_ANSWER)
	String getAnswer();
	
	@JsonProperty(value = Solution.COL_SOLUTION_VERSION)
	Long getSolutionVersion();
	
}
