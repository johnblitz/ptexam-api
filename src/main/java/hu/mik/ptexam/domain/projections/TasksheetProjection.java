package hu.mik.ptexam.domain.projections;

import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hu.mik.ptexam.domain.Exam;
import hu.mik.ptexam.domain.Tasksheet;
import hu.mik.ptexam.domain.TasksheetTask;

@Projection(types = { Tasksheet.class })
@JsonPropertyOrder(alphabetic = false, value = {
	Tasksheet.COL_ID,
	Tasksheet.COL_USERNAME,
	Tasksheet.COL_IS_TERMINATED,
	Tasksheet.COL_TASKSHEET_VERSION,
	Exam.FLD_EXAM,
	TasksheetTask.FLD_TASKSHEET_TASKS
})
public interface TasksheetProjection {

	@JsonProperty(value = Tasksheet.COL_ID)
	Long getId();
	
	@JsonProperty(value = Tasksheet.COL_USERNAME)
	String getUsername();

	@JsonProperty(value = Tasksheet.COL_IS_TERMINATED)
	boolean isTerminated();

	@JsonProperty(value = Tasksheet.COL_TASKSHEET_VERSION)
	Long getTasksheetVersion();

	@JsonProperty(value = Exam.FLD_EXAM)
	ExamProjection getExam();

	@JsonProperty(value = TasksheetTask.FLD_TASKSHEET_TASKS)
	Set<TasksheetTaskProjection> getTasksheetTasks();

}
