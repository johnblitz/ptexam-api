package hu.mik.ptexam.domain.projections;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hu.mik.ptexam.domain.Task;

@Projection(types = { Task.class })
@JsonPropertyOrder(alphabetic = false, value = {
	Task.COL_ID,
	Task.COL_TASK_VERSION,
	Task.COL_QUESTION,
	Task.COL_IS_CODING,
	Task.COL_IS_TEXTUAL
})
public interface TaskProjection {

	@JsonProperty(value = Task.COL_ID)
	String getId();
	
	@JsonProperty(value = Task.COL_QUESTION)
	String getQuestion();
	
	@JsonProperty(value = Task.COL_IS_CODING)
	boolean isCoding();
	
	@JsonProperty(value = Task.COL_IS_TEXTUAL)
	boolean isTextual();
	
	@JsonProperty(value = Task.COL_TASK_VERSION)
	Long getTaskVersion();
	
}
