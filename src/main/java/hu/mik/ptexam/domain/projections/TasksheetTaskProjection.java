package hu.mik.ptexam.domain.projections;

import java.util.Set;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hu.mik.ptexam.domain.Task;
import hu.mik.ptexam.domain.Solution;
import hu.mik.ptexam.domain.TasksheetTask;
import hu.mik.ptexam.domain.TasksheetAnswer;

@Projection(types = { TasksheetTask.class })
@JsonPropertyOrder(alphabetic = false, value = {
	TasksheetTask.COL_ID,
	TasksheetTask.COL_IS_ANSWERED,
	TasksheetTask.COL_TASKSHEET_TASK_VERSION,
	Task.FLD_TASK,
	Solution.FLD_SOLUTIONS,
	TasksheetAnswer.FLD_TASKSHEET_ANSWERS
})
public interface TasksheetTaskProjection {

	@JsonProperty(value = TasksheetTask.COL_ID)
	Long getId();
	
	@JsonProperty(value = TasksheetTask.COL_IS_ANSWERED)
	boolean isAnswered();
	
	@JsonProperty(value = TasksheetTask.COL_TASKSHEET_TASK_VERSION)
	Long getTasksheetTaskVersion();
	
	@JsonProperty(value = Task.FLD_TASK)
	TaskProjection getTask();
	
	@JsonProperty(value = Solution.FLD_SOLUTIONS)
	@Value("#{target.getTask().getSolutions()}")
	Set<SolutionProjection> getPossibleSolutions();
	
	@JsonProperty(value = TasksheetAnswer.FLD_TASKSHEET_ANSWERS)
	List<TasksheetAnswerProjection> getTasksheetAnswers();
	
}
