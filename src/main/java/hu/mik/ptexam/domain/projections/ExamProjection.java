package hu.mik.ptexam.domain.projections;

import java.time.LocalDateTime;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import hu.mik.ptexam.domain.Exam;

@Projection(types = { Exam.class })
@JsonPropertyOrder(alphabetic = false, value = {
	Exam.COL_ID,
	Exam.COL_DESCRIPTION,
	Exam.COL_START_TIME,
	Exam.COL_END_TIME,
	Exam.COL_TASK_COUNT,
	Exam.COL_EXAM_VERSION
})
public interface ExamProjection {

	@JsonProperty(value = Exam.COL_ID)
	String getId();
	
	@JsonProperty(value = Exam.COL_DESCRIPTION)
	String getDescription();
	
	@JsonProperty(value = Exam.COL_START_TIME)
	LocalDateTime getStartTime();
	
	@JsonProperty(value = Exam.COL_END_TIME)
	LocalDateTime getEndTime();
	
	@JsonProperty(value = Exam.COL_TASK_COUNT)
	Integer getTaskCount();
	
	@JsonProperty(value = Exam.COL_EXAM_VERSION)
	Long getExamVersion();
	
}
