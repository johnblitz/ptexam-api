package hu.mik.ptexam.domain.projections;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.rest.core.config.Projection;

import hu.mik.ptexam.domain.*;

@Projection(types = { Result.class })
@JsonPropertyOrder(alphabetic = false, value = {
	Result.COL_ID,
	Result.COL_USERNAME,
	Result.COL_POINTS,
	Result.COL_GRADE,
	Result.COL_RESULT_VERSION,
	Result.COL_CREATED_DATE,
	Result.COL_LAST_MODIFIED_DATE,
	Tasksheet.FLD_TASKSHEET
})
public interface ResultProjection {

	@JsonProperty(value = Result.COL_ID)
	Long getId();
	
	@JsonProperty(value = Result.COL_USERNAME)
	String getUsername();
	
	@JsonProperty(value = Result.COL_POINTS)
	Double getPoints();
	
	@JsonProperty(value = Result.COL_GRADE)
	Integer getGrade();
	
	@JsonProperty(value = Result.COL_RESULT_VERSION)
	Long getResultVersion();
	
	@JsonProperty(value = Result.COL_CREATED_DATE)
	LocalDateTime getCreatedDate();
	
	@JsonProperty(value = Result.COL_LAST_MODIFIED_DATE)
	LocalDateTime getLastModifiedDate();
	
	@JsonProperty(value = Tasksheet.FLD_TASKSHEET)
	TasksheetProjection getTasksheet();
	
}
