package hu.mik.ptexam.domain;

import lombok.Data;

import java.io.Serializable;

import java.time.LocalDateTime;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Version;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import hu.mik.ptexam.utils.IPV4;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@MappedSuperclass
@EntityListeners(value = { AuditingEntityListener.class, AuditingDomainListener.class })
public class Domain implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String GENERATOR = "generator";

	public static final String COL_ID = "id";
	public static final String COL_VERSION = "version";
	public static final String COL_CREATED_DATE = "created_date";
	public static final String COL_LAST_MODIFIED_DATE = "last_modified_date";
	public static final String COL_CREATED_IP = "created_ip";
	public static final String COL_LAST_MODIFIED_IP = "last_modified_ip";
	

	@JsonProperty(value = COL_ID, access = READ_ONLY)
	@Id
	@Column(name = COL_ID, updatable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR)
	private Long id;

	@JsonProperty(value = COL_CREATED_DATE, access = READ_ONLY)
	@Column(name = COL_CREATED_DATE, insertable = true, nullable = false, updatable = false)
	@CreatedDate
	private LocalDateTime createdDate;

	@JsonProperty(value = COL_LAST_MODIFIED_DATE, access = READ_ONLY)
	@Column(name = COL_LAST_MODIFIED_DATE, insertable = true, nullable = false, updatable = true)
	@LastModifiedDate
	private LocalDateTime lastModifiedDate;

	@JsonProperty(value = COL_VERSION, access = READ_ONLY)
	@Column(name = COL_VERSION, insertable = true, nullable = false, updatable = true)
	@Version
	private Long version;

	@JsonProperty(value = COL_CREATED_IP, access = READ_ONLY)
	@Column(name = COL_CREATED_IP, insertable = true, nullable = false, updatable = false)
	private IPV4 createdIp;
	
	@JsonProperty(value = COL_LAST_MODIFIED_IP, access = READ_ONLY)
	@Column(name = COL_LAST_MODIFIED_IP, insertable = true, nullable = false, updatable = true)
	private IPV4 lastModifiedIp;
	
}
