package hu.mik.ptexam.domain;

import java.util.Set;
import java.util.HashSet;

import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Task.TBL_TASKS)
@EqualsAndHashCode(callSuper = false, exclude = { "solutions" })
@GenericGenerator(name = AbstractDomain.GENERATOR, strategy = AbstractDomain.HASH_STRATEGY)
public class Task extends AbstractDomain<String> {

	private static final long serialVersionUID = 1L;

	public static final String TBL_TASKS = "tasks";
	public static final String FLD_TASK = "task";
	public static final String FLD_TASKS = "tasks";
	public static final String COL_QUESTION = "task_question";
	public static final String COL_IS_CODING = "task_is_coding";
	public static final String COL_IS_TEXTUAL = "task_is_textual";
	public static final String COL_TASK_VERSION = "task_version";
	
	@JsonProperty(value = COL_QUESTION, access = READ_WRITE)
	@Column(name = COL_QUESTION, insertable = true, nullable = false, updatable = true, unique = false)
	private String question;
	
	@JsonProperty(value = COL_IS_CODING, access = READ_WRITE)
	@Column(name = COL_IS_CODING, insertable = true, nullable = false, updatable = true, unique = false)
	private boolean coding;
	
	@JsonProperty(value = COL_IS_TEXTUAL, access = READ_WRITE)
	@Column(name = COL_IS_TEXTUAL, insertable = true, nullable = false, updatable = true, unique = false)
	private boolean textual;
	
	@JsonProperty(value = COL_TASK_VERSION, access = READ_ONLY)
	@Transient
	private Long taskVersion;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Topic topic;
	
	@Builder.Default
	@OneToMany(fetch = FetchType.EAGER, mappedBy = FLD_TASK)
	private Set<Solution> solutions = new HashSet<>();
	
	public Long getTaskVersion() {
		return getVersion();
	}
	
}
