package hu.mik.ptexam;

import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { PROJECT_PACKAGE })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone(TIMEZONE));
	}

}
