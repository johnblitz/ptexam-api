package hu.mik.ptexam.controller;

import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.*;
import hu.mik.ptexam.domain.dtos.*;
import static hu.mik.ptexam.ApplicationConstants.*;

@RestController
@RequestMapping(
	path = { "/" + USERS },
	produces = { HAL_PLUS_JSON }
)
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<Object> users() {
		return ResponseEntity.ok(
			new Resources<User>(
				new java.util.ArrayList<>(),
				linkTo(
					methodOn(UserController.class).register(null)
				).withRel(REGISTER),
				linkTo(
					methodOn(UserController.class).users()
				).withSelfRel(),
				linkTo(
					methodOn(IndexController.class).index()
				).withRel(INDEX)
			)
		);
	}
	
	@PostMapping(path = { "/" + REGISTER })
	public ResponseEntity<Object> register(@RequestBody UserDto dto) {
		User user = userService.register(dto);
		return ResponseEntity.ok(
			new Resources<User>(
				user!= null
					? java.util.Arrays.asList(user)
					: new java.util.ArrayList<>(),
				linkTo(
					methodOn(UserController.class).register(dto)
				).withSelfRel(),
				linkTo(
					methodOn(UserController.class).users()
				).withRel(USERS)
			)
		);
	}
	
}
