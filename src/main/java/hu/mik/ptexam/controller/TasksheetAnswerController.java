package hu.mik.ptexam.controller;

import static hu.mik.ptexam.ApplicationConstants.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.core.EmbeddedWrappers;

import hu.mik.ptexam.domain.dtos.AnswerDto;
import hu.mik.ptexam.domain.projections.TasksheetAnswerProjection;
import hu.mik.ptexam.service.TasksheetAnswerService;
import hu.mik.ptexam.service.impl.TasksheetAnswerServiceImpl;

@RepositoryRestController
@RequestMapping(path = { "/" + API_BASE_PATH + "/" + TASKSHEETS }, produces = { HAL_PLUS_JSON })
public class TasksheetAnswerController {

	private final TasksheetAnswerService tasksheetAnswerService;
	private final EmbeddedWrappers wrappers;
	private final ProjectionFactory factory;
	
	@Autowired
	public TasksheetAnswerController(TasksheetAnswerServiceImpl tasksheetAnswerService, ProjectionFactory factory) {
		this.tasksheetAnswerService = tasksheetAnswerService;
		this.wrappers = new EmbeddedWrappers(false);
		this.factory = factory;
	}
	
	@PostMapping(path = { "/{examId}/submit/{tasksheetTaskId}" })
	public ResponseEntity<Object> submit(
		@PathVariable String examId, @PathVariable Long tasksheetTaskId,
		@RequestBody AnswerDto answer) {
		List<TasksheetAnswerProjection> tasksheetAnswers = new ArrayList<>();
		tasksheetAnswerService.submit(examId, tasksheetTaskId, answer).forEach(
			a -> tasksheetAnswers.add(
				factory.createProjection(TasksheetAnswerProjection.class, a)
			)
		);
		Resources<?> result = new Resources<>(
			Arrays.asList(wrappers.wrap(tasksheetAnswers, "submit")),
			linkTo(methodOn(TasksheetAnswerController.class)
				.submit(examId, tasksheetTaskId, answer)).withSelfRel()
		);
		return ResponseEntity.ok(result);
	}
	
}
