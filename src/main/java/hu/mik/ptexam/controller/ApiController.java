package hu.mik.ptexam.controller;

import org.springframework.hateoas.ResourceProcessor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import static hu.mik.ptexam.ApplicationConstants.*;

@BasePathAwareController
@RequestMapping(
	path = { "/" + API_BASE_PATH },
	produces = { HAL_PLUS_JSON }
)
public class ApiController implements
		ResourceProcessor<RepositoryLinksResource> {

	@Override
	public RepositoryLinksResource process(RepositoryLinksResource resource) {
		resource.add(
			linkTo(
				TasksheetController.class
			).withRel(TASKSHEETS),
			linkTo(
				ResultController.class
			).withRel(RESULTS),
			linkTo(
				ApiController.class
			).withSelfRel(),
			linkTo(
				methodOn(IndexController.class).api()
			).withRel(API),
			linkTo(
				methodOn(IndexController.class).index()
			).withRel(INDEX)
		);
		return resource;
	}
	
}
