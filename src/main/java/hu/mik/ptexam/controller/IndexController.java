package hu.mik.ptexam.controller;

import org.springframework.http.HttpStatus;
import org.springframework.hateoas.Resources;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;

import static hu.mik.ptexam.ApplicationConstants.*;

@RestController
@RequestMapping(
	produces = { HAL_PLUS_JSON }
)
public class IndexController extends AbstractErrorController {

	public IndexController(ErrorAttributes errorAttributes) {
		super(errorAttributes);
	}

	@GetMapping(path = { "/" })
	public ResponseEntity<Object> index() {
		return ResponseEntity.ok(
			new Resources<Object>(
				new java.util.ArrayList<>(),
				linkTo(
					methodOn(IndexController.class).api()
				).withRel(API),
				linkTo(
					methodOn(UserController.class).users()
				).withRel(USERS),
				linkTo(
					methodOn(IndexController.class).index()
				).withSelfRel()
			)
		);
	}

	@GetMapping(path = { "/" + API })
	public ResponseEntity<Object> api() {
		return ResponseEntity.ok(
			new Resources<Object>(
				new java.util.ArrayList<>(),
				linkTo(
					ApiController.class
				).withRel(V1),
				linkTo(
					methodOn(IndexController.class).api()
				).withSelfRel(),
				linkTo(
					methodOn(IndexController.class).index()
				).withRel(INDEX)
			)
		);
	}

	@GetMapping(path = { "/" + ERROR })
	public ResponseEntity<Object> error(HttpServletRequest request) {
		java.util.Map<String, Object> body = getErrorAttributes(request, true);
		HttpStatus status = getStatus(request);
		return new ResponseEntity<>(body, status);
	}

	@Override
	public String getErrorPath() {
		return "/" + ERROR;
	}

}
