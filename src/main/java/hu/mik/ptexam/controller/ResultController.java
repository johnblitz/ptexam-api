package hu.mik.ptexam.controller;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.*;
import hu.mik.ptexam.domain.dtos.*;
import hu.mik.ptexam.domain.projections.*;
import static hu.mik.ptexam.ApplicationConstants.*;

@RestController
@RequestMapping(
	path = { "/" + API_BASE_PATH + "/" + RESULTS },
	produces = { HAL_PLUS_JSON }
)
public class ResultController {

	@Autowired
	private ResultService resultService;
	@Autowired
	private ProjectionFactory factory;
	
	@GetMapping
	public ResponseEntity<Object> results() {
		Resources<Result> resources = new Resources<>(
			new java.util.ArrayList<>(),
			linkTo(
				methodOn(ResultController.class).teacherVerification(null, null)
			).withRel(RESULT),
			linkTo(
				methodOn(ResultController.class).search()
			).withRel(SEARCH),
			linkTo(
				methodOn(ResultController.class).results()
			).withSelfRel()
		);
		return ResponseEntity.ok(resources);
	}
	
	@PostMapping(path = { "/{resultId}" })
	public ResponseEntity<Object> teacherVerification(@RequestBody ResultDto x,
			@PathVariable("resultId") Long id) {
		Result result = resultService.teacherVerification(id, x);
		return ResponseEntity.ok(
			new Resource<ResultProjection>(
				factory.createProjection(ResultProjection.class, result),
				linkTo(
					methodOn(ResultController.class).teacherVerification(x, id)
				).withSelfRel()
			)
		);
	}
	
	@GetMapping(path = { "/" + SEARCH })
	public ResponseEntity<Object> search() {
		return ResponseEntity.ok(
			new Resources<Result>(
				new java.util.ArrayList<>(),
				linkTo(
					methodOn(ResultController.class).findAll()
				).withRel(FIND_ALL),
				linkTo(
					methodOn(ResultController.class).findByUsername(null)
				).withRel(FIND_BY_USERNAME),
				linkTo(
					methodOn(ResultController.class).findMyResults()
				).withRel(FIND_MY_RESULTS),
				linkTo(
					methodOn(ResultController.class).search()
				).withSelfRel()
			)
		);
	}
	
	@GetMapping(path = { "/" + SEARCH + "/" + FIND_ALL })
	public ResponseEntity<Object> findAll() {
		java.util.List<ResultProjection> results = new java.util.ArrayList<>();
		resultService.findAll().forEach(result -> results.add(
			factory.createProjection(ResultProjection.class, result)
		));
		return ResponseEntity.ok(
			new Resources<ResultProjection>(
				results,
				linkTo(
					methodOn(ResultController.class).findAll()
				).withSelfRel(),
				linkTo(
					methodOn(ResultController.class).findByUsername(null)
				).withRel(FIND_BY_USERNAME),
				linkTo(
					methodOn(ResultController.class).findMyResults()
				).withRel(FIND_MY_RESULTS),
				linkTo(
					methodOn(ResultController.class).search()
				).withRel(SEARCH)
			)
		);
	}
	
	@GetMapping(path = { "/" + SEARCH + "/" + FIND_BY_USERNAME })
	public ResponseEntity<Object> findByUsername(@RequestParam("username") String username) {
		java.util.List<ResultProjection> results = new java.util.ArrayList<>();
		resultService.findByUsername(username).forEach(result -> results.add(
			factory.createProjection(ResultProjection.class, result)
		));
		return ResponseEntity.ok(
			new Resources<>(
				results,
				linkTo(
					methodOn(ResultController.class).findByUsername(username)
				).withSelfRel(),
				linkTo(
					methodOn(ResultController.class).findAll()
				).withRel(FIND_ALL),
				linkTo(
					methodOn(ResultController.class).findMyResults()
				).withRel(FIND_MY_RESULTS),
				linkTo(
					methodOn(ResultController.class).search()
				).withRel(SEARCH)
			)
		);
	}
	
	@GetMapping(path = { "/" + SEARCH + "/" + FIND_MY_RESULTS })
	public ResponseEntity<Object> findMyResults() {
		java.util.List<ResultProjection> results = new java.util.ArrayList<>();
		resultService.findMyResults().forEach(result -> results.add(
			factory.createProjection(ResultProjection.class, result)
		));
		return ResponseEntity.ok(
			new Resources<>(
				results,
				linkTo(
					methodOn(ResultController.class).findMyResults()
				).withSelfRel(),
				linkTo(
					methodOn(ResultController.class).findAll()
				).withRel(FIND_ALL),
				linkTo(
					methodOn(ResultController.class).findByUsername(null)
				).withRel(FIND_BY_USERNAME),
				linkTo(
					methodOn(ResultController.class).search()
				).withRel(SEARCH)
			)
		);
	}
	
}
