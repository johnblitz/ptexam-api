package hu.mik.ptexam.controller;

import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.*;
import hu.mik.ptexam.domain.projections.*;
import static hu.mik.ptexam.ApplicationConstants.*;

@RestController
@RequestMapping(
	path = { "/" + API_BASE_PATH + "/" + TASKSHEETS },
	produces = { HAL_PLUS_JSON }
)
public class TasksheetController {
	
	@Autowired
	private TasksheetService tasksheetService;
	@Autowired
	private ProjectionFactory factory;
	
	static <T, K> Iterable<T> projection(ProjectionFactory factory,
			Class<T> clazz, K instance) {
		return instance != null
			? java.util.Arrays.asList(factory.createProjection(clazz, instance))
			: new java.util.ArrayList<>();
	}
	
	static <T, K> Iterable<T> projection(ProjectionFactory factory,
			Class<T> clazz, java.util.Optional<K> instance) {
		java.util.List<T> results = new java.util.ArrayList<>();
		instance.ifPresent(
			t -> results.add(factory.createProjection(clazz, t))
		);
		return results;
	}
	
	static <T, K> Iterable<T> projection(ProjectionFactory factory,
			Class<T> clazz, Iterable<K> instances) {
		java.util.List<T> results = new java.util.ArrayList<>();
		instances.forEach(
			t -> results.add(factory.createProjection(clazz, t))
		);
		return results;
	}
	
	@GetMapping
	public ResponseEntity<Object> tasksheets() {
		return ResponseEntity.ok(
			new Resources<Tasksheet>(
				new java.util.ArrayList<>(),
				linkTo(
					methodOn(TasksheetController.class).tasksheets()
				).withSelfRel(),
				linkTo(
					methodOn(TasksheetController.class).get(null)
				).withRel(TASKSHEET),
				linkTo(
					methodOn(TasksheetController.class).terminate(null)
				).withRel(TERMINATE),
				linkTo(
					methodOn(TasksheetAnswerController.class).submit(
						null, null, null)
				).withRel(SUBMIT),
				linkTo(
					methodOn(TasksheetController.class).search()
				).withRel(SEARCH)
			)
		);
	}

	@GetMapping(path = { "/{" + EXAM_ID + "}" })
	public ResponseEntity<Object> get(@PathVariable String examId) {
		return ResponseEntity.ok(
			new Resources<TasksheetProjection>(
				projection(
					this.factory,
					TasksheetProjection.class,
					tasksheetService.provide(examId)
				),
				linkTo(
					methodOn(TasksheetController.class).get(examId)
				).withSelfRel(),
				linkTo(
					methodOn(TasksheetController.class).terminate(examId)
				).withRel(TERMINATE),
				linkTo(
					methodOn(TasksheetAnswerController.class).submit(
						examId, null, null)
				).withRel(SUBMIT)
			)
		);
	}
	
	@GetMapping(path = { "/{" + EXAM_ID + "}/" + TERMINATE })
	public ResponseEntity<Object> terminate(
			@PathVariable(EXAM_ID) String examId) {
		return ResponseEntity.ok(
			new Resources<>(
				projection(
					this.factory,
					ResultProjection.class,
					tasksheetService.terminate(examId)
				),
				linkTo(
					methodOn(TasksheetController.class).terminate(examId)
				).withSelfRel(),
				linkTo(
					methodOn(TasksheetController.class).get(examId)
				).withRel(TASKSHEET)
			)
		);
	}
	
	@GetMapping(path = { "/" + SEARCH })
	public ResponseEntity<Object> search() {
		return ResponseEntity.ok(
			new Resources<Tasksheet>(
				new java.util.ArrayList<>(),
				linkTo(
					methodOn(TasksheetController.class).findAllAvailable()
				).withRel(FIND_ALL_AVAILABLE),
				linkTo(
					methodOn(TasksheetController.class).findAll()
				).withRel(FIND_ALL),
				linkTo(
					methodOn(TasksheetController.class).search()
				).withSelfRel()
			)
		);
	}
	
	@GetMapping(path = { "/" + SEARCH + "/" + FIND_ALL_AVAILABLE })
	public ResponseEntity<Object> findAllAvailable() {
		Resources<Tasksheet> result = new Resources<>(
			new java.util.ArrayList<>(),
			linkTo(
				methodOn(TasksheetController.class).findAllAvailable()
			).withSelfRel(),
			linkTo(
				methodOn(TasksheetController.class).search()
			).withRel(SEARCH)
		);
		tasksheetService.findAllAvailable().forEach(
			tasksheet -> result.add(
				linkTo(
					methodOn(TasksheetController.class).get(
						tasksheet.getExam().getId())
				).withRel(TASKSHEETS)
			)
		);
		return ResponseEntity.ok(result);
	}
	
	@GetMapping(path = { "/" + SEARCH + "/" + FIND_ALL })
	public ResponseEntity<Object> findAll() {
		return ResponseEntity.ok(
			new Resources<TasksheetProjection>(
				projection(
					this.factory,
					TasksheetProjection.class,
					tasksheetService.findAll()
				),
				linkTo(
					methodOn(TasksheetController.class).findAll()
				).withSelfRel(),
				linkTo(
					methodOn(TasksheetController.class).search()
				).withRel(SEARCH)
			)
		);
	}
	
}
