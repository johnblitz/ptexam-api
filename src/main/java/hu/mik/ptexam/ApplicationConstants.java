package hu.mik.ptexam;

public final class ApplicationConstants {

	private ApplicationConstants() {
	}
	
	public static final String PROJECT_PACKAGE = "hu.mik.ptexam";
	public static final String ENTITIES = "hu.mik.ptexam.domain";
	public static final String JPA_REPOSITORIES = "hu.mik.ptexam.dao";
	public static final String UTILS = "hu.mik.ptexam.utils";

	public static final String TIMEZONE = "Europe/Budapest";

	public static final String PROFILE_TEST = "test";
	public static final String PROFILE_DEV = "dev";
	public static final String PROFILE_MASTER = "master";

	public static final String DS_USERNAME = "api";
	public static final String DS_PSSWRD = "szakdolgozat";
	
	public static final String PERSISTENCE_UNIT_NAME = "ptexam";

	/* Links & Domain */
	public static final String API = "api";
	public static final String V1 = "v1";
	public static final String INDEX = "index";
	public static final String USERS = "users";
	public static final String ERROR = "error";
	public static final String REGISTER = "register";
	public static final String API_BASE_PATH = "api/v1";
	public static final String HAL_PLUS_JSON = "application/hal+json";
	public static final String EXAM_ID = "examId";
	public static final String SEARCH = "search";
	public static final String TERMINATE = "terminate";
	public static final String SUBMIT = "submit";
	public static final String FIND_ALL = "findAll";
	public static final String TASKSHEET = "tasksheet";
	public static final String TASKSHEETS = "tasksheets";
	public static final String FIND_ALL_AVAILABLE = "findAllAvailable";
	public static final String RESULT = "result";
	public static final String RESULTS = "results";
	public static final String FIND_BY_USERNAME = "findByUsername";
	public static final String FIND_MY_RESULTS = "findMyResults";

	public static final int ADDITIONAL_TIME = 15;
	
}
