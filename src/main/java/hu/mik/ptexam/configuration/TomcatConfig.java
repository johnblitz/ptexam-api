package hu.mik.ptexam.configuration;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;

import org.springframework.boot.web.server.WebServerFactoryCustomizer;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_TEST, PROFILE_DEV, PROFILE_MASTER })
public class TomcatConfig implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {
	
	@Override
	public void customize(TomcatServletWebServerFactory factory) {
		factory
			.addConnectorCustomizers(connector -> 
				connector.setAttribute(
					"relaxedQueryChars",
					"[]|{}^&#x5c;&#x60;&quot;&lt;&gt;"
				)
			);
	}

}
