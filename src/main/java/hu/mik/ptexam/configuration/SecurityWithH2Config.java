package hu.mik.ptexam.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.authentication.dao.DaoAuthenticationProvider;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import hu.mik.ptexam.service.impl.CustomUserDetailsService;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_DEV, PROFILE_TEST })
@EnableWebSecurity
public class SecurityWithH2Config extends WebSecurityConfigurerAdapter {
	
	private static final String ROLE_ADMIN = "ADMIN";
	private static final String ROLE_TEACHER = "TEACHER";
	private static final String ROLE_STUDENT = "STUDENT";
	
	private final CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	public SecurityWithH2Config(CustomUserDetailsService customUserDetailsService) {
		this.customUserDetailsService = customUserDetailsService;
	}
	
	@Bean
	public DaoAuthenticationProvider customAuthenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(customUserDetailsService);
		provider.setPasswordEncoder(encoder());
		return provider;
	}
	
	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(customAuthenticationProvider());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors().and()
			.csrf().disable()
			.httpBasic().and()
			.headers()
				.frameOptions().sameOrigin()
				.and()
			.authorizeRequests()
				.antMatchers(
					"/h2/**",
					"/users",
					"/users/register"
				).hasRole(ROLE_ADMIN)
				.antMatchers(
					"/api/v1/subjects/**",
					"/api/v1/topics/**",
					"/api/v1/tasks/**",
					"/api/v1/solutions/**",
					"/api/v1/classrooms/**",
					"/api/v1/exams/**",
					"/api/v1/results/**"
				).hasAnyRole(ROLE_TEACHER, ROLE_ADMIN)
				.antMatchers(
					"/api/v1/tasksheets",
					"/api/v1/results",
					"/api/v1/results/search"
				).hasAnyRole(ROLE_TEACHER, ROLE_STUDENT, ROLE_ADMIN)
				.antMatchers(
					"/api/v1/tasksheets/search/**",
					"/api/v1/tasksheets/{exam_id}/**",
					"/api/v1/results/search/findMyResults"
				).hasAnyRole(ROLE_STUDENT, ROLE_ADMIN)
				.anyRequest().authenticated();
	}

}
