package hu.mik.ptexam.configuration;

import java.util.*;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import static java.util.Arrays.asList;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import hu.mik.ptexam.dao.*;
import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.utils.IPV4;
import static hu.mik.ptexam.ApplicationConstants.*;

@Log4j2
@Configuration
@Profile(value = { PROFILE_DEV, PROFILE_TEST })
public class H2DataLoader implements ApplicationRunner {

	private static final String SINGLE_SELECT_TASK =
		"Egy/Több megoldásos feladat -> egy megoldásos";
	private static final String MULTI_SELECT_TASK =
		"Egy/Több megoldásos feladat -> több megoldásos";
	private static final String TEXTUAL_TASK =
		"Szöveges megoldásos feladat";
	private static final String CODING_TASK = "Programozási feladat";
	
	private static final String SINGLE_CORRECT_SOLUTION =
		"Az egyetlen jó megoldás";
	private static final String MULTI_CORRECT_SOLUTION =
		"Egy jó megoldás a sokból";
	private static final String INCORRECT_SOLUTION = "Rossz megoldás";
	
	@Autowired ClassroomRepository classroomRepository;
	@Autowired SolutionRepository solutionRepository;
	@Autowired SubjectRepository subjectRepository;
	@Autowired TopicRepository topicRepository;
	@Autowired ExamRepository examRepository;
	@Autowired TaskRepository taskRepository;
	
	@Autowired PrivilegeRepository privilegeRepository;
	@Autowired UserRepository userRepository;
	@Autowired RoleRepository roleRepository;
	
	@Autowired PasswordEncoder encoder;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("Filling up H2 database with users, roles and privileges");
		Set<Privilege> privileges = new HashSet<>(asList(
			Privilege.builder()
				.name("TEACHER_PRIVILEGE")
				.build(),
			Privilege.builder()
				.name("STUDENT_PRIVILEGE")
				.build()
		));
		privilegeRepository.saveAll(privileges);
		Iterator<Privilege> privilegesIterator = privileges.iterator();
		Set<Role> roles = new HashSet<>(asList(
			Role.builder()
				.name("ROLE_ADMIN")
				.privileges(privileges)
				.build(),
			Role.builder()
				.name("ROLE_TEACHER")
				.privileges(new HashSet<>(asList(privilegesIterator.next())))
				.build(),
			Role.builder()
				.name("ROLE_STUDENT")
				.privileges(new HashSet<>(asList(privilegesIterator.next())))
				.build()
		));
		roleRepository.saveAll(roles);
		Iterator<Role> rolesIterator = roles.iterator();
		Role admin = rolesIterator.next();
		Role student = rolesIterator.next();
		Role teacher = rolesIterator.next();
		Set<User> users = new HashSet<>(asList(
			User.builder()
				.username("SysAdmin")
				.lastName("Balla")
				.firstName("Dávid")
				.password(encoder.encode("n-8B7+6a"))
				.accountNonExpired(true)
				.accountNonLocked(true)
				.credentialsNonExpired(true)
				.enabled(true)
				.roles(new HashSet<>(asList(admin)))
				.build(),
			User.builder()
				.username("teacher")
				.password(encoder.encode("teacher"))
				.accountNonExpired(true)
				.accountNonLocked(true)
				.credentialsNonExpired(true)
				.enabled(true)
				.roles(new HashSet<>(asList(teacher)))
				.build(),
			User.builder()
				.username("student")
				.password(encoder.encode("student"))
				.accountNonExpired(true)
				.accountNonLocked(true)
				.credentialsNonExpired(true)
				.enabled(true)
				.roles(new HashSet<>(asList(student)))
				.build()
		));
		userRepository.saveAll(users);
		log.info("Filling up H2 database with dummy data");
		List<Subject> subjects = asList(
			Subject.builder()
				.code("PMTMINB238H")
				.name("Szakdolgozat")
				.build(),
			Subject.builder()
				.code("PMTMINB216H")
				.name("Intelligens rendszerek II.")
				.build(),
			Subject.builder()
				.code("PMTRTNB336H")
				.name("Hálózat- és rendszermenedzsment")
				.build()
		);
		subjectRepository.saveAll(subjects);
		List<Topic> topics = asList(
			Topic.builder()
				.name("szakdolgozat_default")
				.subject(subjects.get(0))
				.build(),
			Topic.builder()
				.name("int2_default")
				.subject(subjects.get(1))
				.build(),
			Topic.builder()
				.name("hrm_default")
				.subject(subjects.get(2))
				.build()
		);
		topicRepository.saveAll(topics);
		List<Task> tasks = asList(
			Task.builder()
				.question(SINGLE_SELECT_TASK)
				.coding(false).textual(false)
				.topic(topics.get(0)).build(),
			Task.builder()
				.question(MULTI_SELECT_TASK)
				.coding(false).textual(false)
				.topic(topics.get(0)).build(),
			Task.builder()
				.question(CODING_TASK)
				.coding(true).textual(false)
				.topic(topics.get(0)).build(),
			Task.builder()
				.question(TEXTUAL_TASK)
				.coding(false).textual(true)
				.topic(topics.get(0)).build(),
			Task.builder()
				.question(SINGLE_SELECT_TASK)
				.coding(false).textual(false)
				.topic(topics.get(1)).build(),
			Task.builder()
				.question(MULTI_SELECT_TASK)
				.coding(false).textual(false)
				.topic(topics.get(1)).build(),
			Task.builder()
				.question(CODING_TASK)
				.coding(true).textual(false)
				.topic(topics.get(1)).build(),
			Task.builder()
				.question(TEXTUAL_TASK)
				.coding(false).textual(true)
				.topic(topics.get(1)).build(),
			Task.builder()
				.question(SINGLE_SELECT_TASK)
				.coding(false).textual(false)
				.topic(topics.get(2)).build(),
			Task.builder()
				.question(MULTI_SELECT_TASK)
				.coding(false).textual(false)
				.topic(topics.get(2)).build(),
			Task.builder()
				.question(CODING_TASK)
				.coding(true).textual(false)
				.topic(topics.get(2)).build(),
			Task.builder()
				.question(TEXTUAL_TASK)
				.coding(false).textual(true)
				.topic(topics.get(2)).build()
		);
		taskRepository.saveAll(tasks);
		List<Solution> solutions = asList(
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(0)).build(),
			Solution.builder()
				.answer(SINGLE_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(0)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(0)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(1)).build(),
			Solution.builder()
				.answer(MULTI_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(1)).build(),
			Solution.builder()
				.answer(MULTI_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(1)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(1)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(4)).build(),
			Solution.builder()
				.answer(SINGLE_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(4)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(4)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(5)).build(),
			Solution.builder()
				.answer(MULTI_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(5)).build(),
			Solution.builder()
				.answer(MULTI_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(5)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(5)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(8)).build(),
			Solution.builder()
				.answer(SINGLE_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(8)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(8)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(9)).build(),
			Solution.builder()
				.answer(MULTI_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(9)).build(),
			Solution.builder()
				.answer(MULTI_CORRECT_SOLUTION).correct(true)
				.task(tasks.get(9)).build(),
			Solution.builder()
				.answer(INCORRECT_SOLUTION).correct(false)
				.task(tasks.get(9)).build()
		);
		solutionRepository.saveAll(solutions);
		List<Classroom> classrooms = asList(
			Classroom.builder()
				.location("Universal IP room for testing api")
				.ipStart(new IPV4(0, 0, 0, 0))
				.ipEnd(new IPV4(255, 255, 255, 255))
				.build(),
			Classroom.builder()
				.location("A201")
				.ipStart(new IPV4(192, 168, 0, 2))
				.ipEnd(new IPV4(192, 168, 0, 254))
				.build(),
			Classroom.builder()
				.location("A202")
				.ipStart(new IPV4(192, 168, 1, 2))
				.ipEnd(new IPV4(192, 168, 1, 254))
				.build(),
			Classroom.builder()
				.location("A203")
				.ipStart(new IPV4(192, 168, 2, 2))
				.ipEnd(new IPV4(192, 168, 2, 254))
				.build(),
			Classroom.builder()
				.location("A214")
				.ipStart(new IPV4(192, 168, 3, 2))
				.ipEnd(new IPV4(192, 168, 3, 254))
				.build(),
			Classroom.builder()
				.location("A216")
				.ipStart(new IPV4(192, 168, 4, 2))
				.ipEnd(new IPV4(192, 168, 4, 254))
				.build()
		);
		classroomRepository.saveAll(classrooms);
		List<Exam> exams = asList(
			Exam.builder()
				.description("Univerzális próba zh")
				.startTime(LocalDateTime.of(2019, 4, 1, 0, 0, 0))
				.endTime(LocalDateTime.of(2019, 4, 30, 23, 59, 59))
				.taskCount(3).classroom(classrooms.get(0)).topic(topics.get(0))
				.build(),
			Exam.builder()
				.description("próba zh, határidő: 2019-04-30 23:59:59")
				.startTime(LocalDateTime.of(2019, 4, 1, 0, 0, 0))
				.endTime(LocalDateTime.of(2019, 4, 30, 23, 59, 59))
				.taskCount(3).classroom(classrooms.get(2)).topic(topics.get(0))
				.build(),
			Exam.builder()
				.description("próba zh 2, határidő: 2019-05-30 23:59:59")
				.startTime(LocalDateTime.of(2019, 5, 1, 0, 0, 0))
				.endTime(LocalDateTime.of(2019, 5, 30, 23, 59, 59))
				.taskCount(3).classroom(classrooms.get(2)).topic(topics.get(0))
				.build(),
			Exam.builder()
				.description("Innobyte Teszt ZH")
				.startTime(LocalDateTime.of(2019, 6, 1, 0, 0, 0))
				.endTime(LocalDateTime.of(2019, 6, 30, 23, 59, 59))
				.taskCount(3).classroom(classrooms.get(0)).topic(topics.get(0))
				.build()
		);
		examRepository.saveAll(exams);
	}

}
