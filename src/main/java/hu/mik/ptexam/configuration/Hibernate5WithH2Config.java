package hu.mik.ptexam.configuration;

import java.util.Properties;
import com.zaxxer.hikari.HikariDataSource;
import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.*;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_DEV, PROFILE_TEST })
@EnableJpaRepositories(basePackages = { JPA_REPOSITORIES })
@EntityScan(basePackages = { ENTITIES })
@EnableTransactionManagement
@EnableJpaAuditing
public class Hibernate5WithH2Config {

	@Bean(name = "jpaVendorAdapter")
	JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect");
		return adapter;
	}
	
	@SuppressWarnings("all")
	@Bean(name = "dataSource")
	HikariDataSource dataSource() {
		HikariDataSource ds = new HikariDataSource();
		ds.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
		ds.addDataSourceProperty("url", "jdbc:h2:mem:ptexam;DB_CLOSE_DELAY=-1");
		ds.addDataSourceProperty("user", DS_USERNAME);
		ds.addDataSourceProperty("password", DS_PSSWRD);
		return ds;
	}

	@Bean(name = { "jpaProperties" })
	Properties jpaProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", false);
		properties.put("hibernate.hbm2ddl.auto", "create-drop");
		properties.put("hibernate.jdbc.lob.non_contextual_creation", true);
		return properties;
	}

	@Bean(name = { "entityManagerFactory" })
	EntityManagerFactory entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean emf =
				new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(dataSource());
		emf.setJpaVendorAdapter(jpaVendorAdapter());
		emf.setPackagesToScan(ENTITIES, UTILS);
		emf.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
		emf.setJpaProperties(jpaProperties());
		emf.afterPropertiesSet();
		return emf.getObject();
	}

}
