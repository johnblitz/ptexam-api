package hu.mik.ptexam.configuration;

import org.springframework.web.cors.*;
import static java.util.Arrays.asList;
import org.springframework.web.filter.CorsFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_TEST, PROFILE_DEV, PROFILE_MASTER })
public class CORSConfig {

	@Bean(name = { "corsFilter" })
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source =
			new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.setAllowedMethods(asList(
			"OPTIONS", "GET", "POST", "PUT", "DELETE"
		));
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

}
