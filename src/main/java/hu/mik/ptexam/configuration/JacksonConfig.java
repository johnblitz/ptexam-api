package hu.mik.ptexam.configuration;

import java.time.*;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.datatype.jsr310.ser.*;
import com.fasterxml.jackson.datatype.jsr310.deser.*;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_TEST, PROFILE_DEV, PROFILE_MASTER })
public class JacksonConfig {

	private static final DateTimeFormatter FORMATTER =
			DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Bean(name = { "objectMapper" })
	ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		JavaTimeModule javaTimeModule = new JavaTimeModule();
		javaTimeModule.addSerializer(
				LocalDateTime.class,
				new LocalDateTimeSerializer(FORMATTER));
		javaTimeModule.addDeserializer(
				LocalDateTime.class,
				new LocalDateTimeDeserializer(FORMATTER));
		javaTimeModule.addSerializer(
				LocalDate.class,
				new LocalDateSerializer(FORMATTER));
		javaTimeModule.addDeserializer(
				LocalDate.class,
				new LocalDateDeserializer(FORMATTER));
		mapper.registerModule(javaTimeModule);
		return mapper;
	}

}
