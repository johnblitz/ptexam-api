package hu.mik.ptexam.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import static org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy.RepositoryDetectionStrategies.*;

import static hu.mik.ptexam.ApplicationConstants.*;

import hu.mik.ptexam.domain.*;

@Configuration
@Profile(value = { PROFILE_TEST, PROFILE_DEV, PROFILE_MASTER })
public class RestConfig implements RepositoryRestConfigurer {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.setBasePath("/api/v1");
		config.setRepositoryDetectionStrategy(ANNOTATED);
		config.exposeIdsFor(
			Subject.class,
			Topic.class,
			Task.class,
			Solution.class,
			Classroom.class,
			Exam.class,
			Tasksheet.class,
			TasksheetTask.class,
			TasksheetAnswer.class
		);
	}

	@Bean(name = { "projectionFactory" })
	SpelAwareProxyProjectionFactory projectionFactory() {
		return new SpelAwareProxyProjectionFactory();
	}

}
