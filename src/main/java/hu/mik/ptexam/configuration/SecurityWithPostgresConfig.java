package hu.mik.ptexam.configuration;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_MASTER })
@EnableWebSecurity
public class SecurityWithPostgresConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.requiresChannel()
				.anyRequest().requiresSecure();
	}
	
}
