package hu.mik.ptexam.configuration;

import java.util.Properties;

import com.zaxxer.hikari.HikariDataSource;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Configuration;

import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.transaction.annotation.EnableTransactionManagement;

import static hu.mik.ptexam.ApplicationConstants.*;

@Configuration
@Profile(value = { PROFILE_MASTER })
@EnableJpaRepositories(basePackages = { JPA_REPOSITORIES })
@EntityScan(basePackages = { ENTITIES })
@EnableTransactionManagement
public class Hibernate5WithPostgresConfig {

	@Bean(name = "jpaVendorAdapter")
	JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQL9Dialect");
		return adapter;
	}

	@Bean(name = "jpaProperties")
	Properties jpaProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", false);
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.jdbc.lob.non_contextual_creation", true);
		return properties;
	}

	@SuppressWarnings("all")
	@Bean(name = "dataSource")
	HikariDataSource dataSource() {
		HikariDataSource ds = new HikariDataSource();
		ds.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
		ds.addDataSourceProperty("url", "jdbc:postgresql://localhost:32401/ptexam");
		ds.addDataSourceProperty("user", "postgres");
		ds.addDataSourceProperty("password", "szakdolgozat");
		return ds;
	}

	@Bean(name = "entityManagerFactory")
	EntityManagerFactory entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean emf =
				new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(dataSource());
		emf.setJpaVendorAdapter(jpaVendorAdapter());
		emf.setPackagesToScan(ENTITIES);
		emf.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
		emf.setJpaProperties(jpaProperties());
		emf.afterPropertiesSet();
		return emf.getObject();
	}

}
