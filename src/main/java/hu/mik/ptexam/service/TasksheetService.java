package hu.mik.ptexam.service;

import hu.mik.ptexam.domain.Result;
import hu.mik.ptexam.domain.Tasksheet;

public interface TasksheetService {

	Iterable<Tasksheet> findAllAvailable();
	
	Iterable<Tasksheet> findAll();
	
	java.util.Optional<Tasksheet> provide(String exam);
	
	Result terminate(String exam);
	
}
