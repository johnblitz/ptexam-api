package hu.mik.ptexam.service;

import hu.mik.ptexam.domain.Result;
import hu.mik.ptexam.domain.Tasksheet;
import hu.mik.ptexam.domain.dtos.ResultDto;

public interface ResultService {

	Iterable<Result> findAll();
	
	Iterable<Result> findByUsername(String username);
	
	Iterable<Result> findMyResults();
	
	Result verify(Tasksheet tasksheet);
	
	Result teacherVerification(Long resultId, ResultDto dto);
	
}
