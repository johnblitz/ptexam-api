package hu.mik.ptexam.service;

import hu.mik.ptexam.domain.User;
import hu.mik.ptexam.domain.dtos.UserDto;

public interface UserService {

	User register(UserDto dto);
	
}
