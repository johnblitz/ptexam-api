package hu.mik.ptexam.service;

import hu.mik.ptexam.domain.Tasksheet;
import hu.mik.ptexam.domain.TasksheetTask;

public interface TasksheetTaskService {
	
	Iterable<TasksheetTask> generateTasks(Tasksheet tasksheet);
	
}
