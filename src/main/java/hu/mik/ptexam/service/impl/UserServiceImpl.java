package hu.mik.ptexam.service.impl;

import static java.util.Arrays.asList;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import hu.mik.ptexam.dao.RoleRepository;
import hu.mik.ptexam.dao.UserRepository;
import hu.mik.ptexam.domain.Role;
import hu.mik.ptexam.domain.User;
import hu.mik.ptexam.domain.dtos.UserDto;
import hu.mik.ptexam.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder encoder;

	/**
	 * This method registers a new user to the system.
	 * @param dto User class DTO.
	 * @return Registered user.
	 */
	@Override
	public User register(UserDto dto) {
		User user = userRepository.findByUsername(dto.getUsername());
		Role role = roleRepository.findByName(dto.getRole());
		
		if (role == null) return null;
		
		if (user != null) {
			user.setPassword(encoder.encode(dto.getPassword()));
			user.setRoles(new HashSet<>(asList(role)));
			return userRepository.save(user);
		} else {
			return userRepository.save(
				User.builder()
					.username(dto.getUsername())
					.password(encoder.encode(dto.getPassword()))
					.accountNonExpired(true)
					.accountNonLocked(true)
					.credentialsNonExpired(true)
					.enabled(true)
					.email(null)
					.firstName(null)
					.lastName(null)
					.roles(new HashSet<>(asList(role)))
					.build()
			);
		}
	}

}
