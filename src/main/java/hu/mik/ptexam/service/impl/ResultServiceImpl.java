package hu.mik.ptexam.service.impl;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.domain.dtos.ResultDto;
import hu.mik.ptexam.dao.ResultRepository;
import hu.mik.ptexam.service.ResultService;
import static hu.mik.ptexam.service.Validation.*;
import static hu.mik.ptexam.utils.Simplification.*;

@Log4j2
@Service
public class ResultServiceImpl implements ResultService {

	@Autowired
	private ResultRepository resultRepository;
	
	private Tasksheet tasksheet;

	/**
	 * This method lists all results recorded.
	 * @return All-time results.
	 */
	@Override
	public Iterable<Result> findAll() {
		return resultRepository.findAll();
	}

	/**
	 * This method lists results by student's username.
	 * @param username Student's name.
	 * @return Student's results.
	 */
	@Override
	public Iterable<Result> findByUsername(String username) {
		return resultRepository.findByUsername(username);
	}

	/**
	 * This method lists current student's results.
	 * @return Student's results.
	 */
	@Override
	public Iterable<Result> findMyResults() {
		try {
			String username = isUserValid();
			return resultRepository.findByUsername(username);
		}
		catch (NullPointerException | IllegalStateException e) {
			log.warn(e.getMessage());
			return new ArrayList<>();
		}
	}
	
	private double point(List<Solution> marked, Iterator<Solution> iterator) {
		if (marked.isEmpty()) return 0;
		int sum = 0;
		
		while(iterator.hasNext()) {
			Solution s = iterator.next();
			if (equalsAny(s, marked)) sum += s.isCorrect() ? 1 : -1;
		}
		int total = Math.max(0, sum);
		
		return total / (double) marked.size();
	}
	
	private double points() {
		double points = 0F;
		
		for (TasksheetTask task : tasksheet.getTasksheetTasks()) {
			if (!isCoding(task) && !isTextual(task)) {
				List<Solution> marked = new ArrayList<>();
				task.getTasksheetAnswers()
					.forEach(a -> marked.add(a.getSolution()));
				points +=
					point(marked, task.getTask().getSolutions().iterator());
			}
		}
		
		return points;
	}

	/**
	 * This method is a system used service method, to auto-correct tasksheets.
	 * @param tasksheet A tasksheet to verify.
	 * @return An early access result.
	 */
	@Override
	public Result verify(Tasksheet tasksheet) {
		try {
			this.tasksheet = requireNonNull(tasksheet);
			isTerminated(tasksheet, true);
			String username = isUserValid();
			require(tasksheet.getUsername().equals(username));
			
			Result result = resultRepository
				.findByUsernameAndTasksheetId(username, tasksheet.getId());
			
			if (result == null) {
				result = resultRepository.save(
					Result.builder()
						.username(username)
						.grade(0)
						.points(points())
						.tasksheet(tasksheet)
						.build()
				);
			}
			
			return result;
		}
		catch (NullPointerException | IllegalStateException e) {
			log.warn(e.getMessage());
			return null;
		}
	}

	/**
	 * This method is for teachers to verify each student's exam.
	 * @param id A result identifier.
	 * @param x A Result DTO.
	 * @return Verified result.
	 */
	@Override
	public Result teacherVerification(Long id, ResultDto x) {
		try {
			Result result = requireNonNull(resultRepository.findById(id));
			
			result.setGrade(x.getGrade());
			result.setPoints(x.getPoints());
			
			return resultRepository.save(result);
		}
		catch (NullPointerException | IllegalStateException e) {
			log.warn(e.getMessage());
			return null;
		}
	}

}
