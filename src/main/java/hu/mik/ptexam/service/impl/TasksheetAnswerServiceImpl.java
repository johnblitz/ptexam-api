package hu.mik.ptexam.service.impl;

import java.util.List;
import java.util.Iterator;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import hu.mik.ptexam.dao.*;
import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.*;
import hu.mik.ptexam.utils.IPV4;
import hu.mik.ptexam.domain.dtos.*;
import static hu.mik.ptexam.service.Validation.*;
import static hu.mik.ptexam.ApplicationConstants.*;
import static hu.mik.ptexam.utils.Simplification.*;

@Log4j2
@Service
public class TasksheetAnswerServiceImpl implements TasksheetAnswerService {
	
	private static final String SUBMITTING_TEMPLATE =
		"Submitting answer(s) comment=%s, answers=%d, from IP=%s";
	private static final String STUDENT_LATE_TEMPLATE =
		"Student is late=%s, by=%d seconds";
	
	@Autowired
	private TasksheetAnswerRepository tasksheetAnswerRepository;
	@Autowired
	private TasksheetTaskRepository tasksheetTaskRepository;
	
	private TasksheetTask tasksheetTask;
	private TasksheetAnswer temp;
	
	private void add(String c, Solution s, long dt, String u) {
		final boolean late = dt > 0L;
		tasksheetAnswerRepository.save(
			TasksheetAnswer.builder()
				.comment(c)
				.solution(s)
				.tasksheetTask(tasksheetTask)
				.username(u)
				.late(late)
				.latenessInSeconds(dt)
				.build()
		);
	}
	
	private void update(String c, Solution s, long dt, String u) {
		final boolean late = dt > 0L;
		temp.setComment(c);
		temp.setSolution(s);
		temp.setLate(late);
		temp.setUsername(u);
		temp.setLatenessInSeconds(dt);
		tasksheetAnswerRepository.save(temp);
	}
	
	private void save(String c, Solution s, long dt, String u) {
		if (tasksheetTask.getTasksheetAnswers().isEmpty()) {
			add(c, s, dt, u);
		}
		else {
			if (isTextual(tasksheetTask) || isCoding(tasksheetTask)) {
				Iterator<TasksheetAnswer> iterator =
					tasksheetTask.getTasksheetAnswers().iterator();
				temp = iterator.next();
				update(c, s, dt, u);
			}
			else {
				final long count = tasksheetTask.getTasksheetAnswers().stream()
					.filter(a -> a.getSolution().getId().equals(s.getId()))
					.count();
				if (count == 0)
				{
					add(c, s, dt, u);
				}
				else {
					tasksheetTask.getTasksheetAnswers().forEach(a -> {
						if (a.getSolution().getId().equals(s.getId()))
						{ temp = a; update(c, s, dt, u); }
					});
				}
			}
		}
	}
	
	private void submit(long dt, AnswerDto x, String u, boolean advanced) {
		final Long id = tasksheetTask.getId();
		List<TasksheetAnswer> answers = tasksheetAnswerRepository
			.findByTasksheetTaskIdAndUsername(id, u);
		boolean answered = false;
		
		try {
			if (advanced) requireNonNull(x.getSolutions()); 
			else requireNonNull(x.getComment()); 
			
			log.info(String.format(STUDENT_LATE_TEMPLATE, dt > 0L, dt));
			
			if (advanced) {
				tasksheetTask.getTask().getSolutions().forEach(s -> {
					if (equalsAny(s.getId(), x.getSolutions()))
					{ save(x.getComment(), s, dt, u); }
				});
				tasksheetTask.getTasksheetAnswers().forEach(a -> {
					if (!equalsAny(a.getSolution().getId(), x.getSolutions()))
					{ tasksheetAnswerRepository.delete(a); }
				});
			}
			else
			{ save(x.getComment(), null, dt, u); }
			
			answered = true;
		}
		catch (NullPointerException e) {
			answers.forEach(tasksheetAnswerRepository::delete);
			tasksheetTask.setTasksheetAnswers(new java.util.HashSet<>());
			answered = false;
		}
		
		tasksheetTask.setAnswered(answered);
		this.tasksheetTask = tasksheetTaskRepository.save(tasksheetTask);
	}

	/**
	 * This method is for submitting answers by students.
	 * @param eId An exam identifier.
	 * @param tId A tasksheet task identifier.
	 * @param x Tasksheet answer DTO.
	 * @return Submitted answer(s).
	 */
	@Override
	public Iterable<TasksheetAnswer> submit(String eId, Long tId, AnswerDto x) {
		try {
			this.tasksheetTask = requireNonNull(
				tasksheetTaskRepository.findById(tId)
			);
			Tasksheet tasksheet = requireNonNull(
				tasksheetTask.getTasksheet()
			);
			Exam exam = requireNonNull(
				tasksheet.getExam()
			);
			Classroom classroom = requireNonNull(
				exam.getClassroom()
			);
			
			final LocalDateTime current = isInTime(exam, ADDITIONAL_TIME);
			final IPV4 address = isInValidIpRange(classroom);
			final String username = isUserValid();
			require(tasksheet.getUsername().equals(username));
			require(exam.getId().equals(eId));
			isTerminated(tasksheet, false);
			
			log.info(String.format(SUBMITTING_TEMPLATE,
				x.getComment(), x.getSolutions().size(), address
			));
			
			final long timeLeft = remainingTime(exam, current);
			final boolean advanced =
				!isCoding(tasksheetTask) && !isTextual(tasksheetTask);
			submit(timeLeft, x, username, advanced);
			
			return tasksheetAnswerRepository.findByTasksheetTaskIdAndUsername(
				tasksheetTask.getId(), username
			);
		}
		catch (NullPointerException | IllegalStateException e) {
			log.warn(e.getMessage());
			return new java.util.ArrayList<>();
		}
	}
	
}
