package hu.mik.ptexam.service.impl;

import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import hu.mik.ptexam.dao.*;
import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.CustomUserPrincipal;

@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	private final UserRepository userRepository;
	
	@Autowired
	public CustomUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * This method loads user and his/her details to Spring authentication from database.
	 * @param username User's name.
	 * @return User's details.
	 */
	@Override
	public UserDetails loadUserByUsername(String username) {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		
		return new CustomUserPrincipal(user);
	}
	
}
