package hu.mik.ptexam.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import hu.mik.ptexam.dao.*;
import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.*;
import hu.mik.ptexam.utils.IPV4;
import static hu.mik.ptexam.service.Validation.*;
import static hu.mik.ptexam.utils.Simplification.*;
import static hu.mik.ptexam.ApplicationConstants.*;

@Log4j2
@Service
public class TasksheetServiceImpl implements TasksheetService {

	private static final String PROVIDING_TEMPLATE =
		"Providing task sheet to IP=%s, Username=%s for Exam=%s";
	
	@Autowired
	private TasksheetTaskService tasksheetTaskService;
	@Autowired
	private TasksheetRepository tasksheetRepository;
	@Autowired
	private ExamRepository examRepository;
	@Autowired
	private ResultService resultService;

	/**
	 * This method lists all available tasksheets.
	 * @return All available tasksheets.
	 */
	@Override
	public Iterable<Tasksheet> findAllAvailable() {
		java.util.List<Tasksheet> tasksheets = new java.util.ArrayList<>();
		examRepository.findAll().forEach(
			exam -> tasksheets.add(
				Tasksheet.builder()
					.exam(exam)
					.terminated(false)
					.build()
			)
		);
		return tasksheets;
	}

	/**
	 * This method lists all tasksheets.
	 * @return All tasksheets.
	 */
	@Override
	public Iterable<Tasksheet> findAll() {
		return tasksheetRepository.findAll();
	}

	/**
	 * This method provides an optional tasksheet for a given exam identifier.
	 * @param examId An exam identifier.
	 * @return An optional tasksheet.
	 */
	@Override
	public java.util.Optional<Tasksheet> provide(String examId) {
		try {
			Exam exam = requireNonNull(examRepository.findById(examId));
			Classroom classroom = requireNonNull(exam.getClassroom());
			
			isInTime(exam, ADDITIONAL_TIME);
			IPV4 address = isInValidIpRange(classroom);
			String username = isUserValid();
			
			java.util.Optional<Tasksheet> tasksheet = tasksheetRepository
				.findByExamIdAndUsername(examId, username);
			if (tasksheet.isPresent() && !isTerminated(tasksheet.get(), false))
				return tasksheet;
			
			log.info(String.format(
				PROVIDING_TEMPLATE, address, username, examId
			));
			
			tasksheet = java.util.Optional.of(
				tasksheetRepository.save(
					Tasksheet.builder()
						.exam(exam)
						.username(username)
						.terminated(false)
						.build()
				)
			);
			
			tasksheetTaskService.generateTasks(tasksheet.get()).forEach(
				tasksheet.get().getTasksheetTasks()::add
			);
			
			return tasksheet;
		}
		catch (NullPointerException | IllegalStateException e) {
			log.warn(e.getMessage());
			return java.util.Optional.empty();
		}
	}

	/**
	 * This method terminates the exam, which means, that the student can't submit an answer anymore.
	 * After termination, the system auto verifies the correctable answers and returns an early access result.
	 * Early access result means, that the teacher, must verify the final grade and points.
	 * @param examId An exam identifier.
	 * @return An exam result.
	 */
	@Override
	public Result terminate(String examId) {
		try {
			Exam exam = requireNonNull(examRepository.findById(examId));
			Classroom classroom = requireNonNull(exam.getClassroom());
			
			isInTime(exam, ADDITIONAL_TIME);
			isInValidIpRange(classroom);
			String username = isUserValid();
			
			Tasksheet tasksheet = requireNonNull(tasksheetRepository
				.findByExamIdAndUsername(examId, username));
			
			tasksheet.setTerminated(true);
			
			return resultService.verify(tasksheetRepository.save(tasksheet));
		}
		catch (NullPointerException | IllegalStateException e) {
			log.warn(e.getMessage());
			return null;
		}
	}

}
