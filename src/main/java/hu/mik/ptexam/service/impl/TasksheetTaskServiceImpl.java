package hu.mik.ptexam.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import hu.mik.ptexam.dao.*;
import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.service.TasksheetTaskService;

@Log4j2
@Service
public class TasksheetTaskServiceImpl implements TasksheetTaskService {

	private static final String GENERATING_TEMPLATE =
		"Generating tasks for tasksheet id=%d, username=%s";
	
	@Autowired
	private TasksheetTaskRepository tasksheetTaskRepository;
	@Autowired
	private TaskRepository taskRepository;

	/**
	 * This method generates the tasksheet tasks.
	 * Randomization and selection happens here.
	 * @param tasksheet A tasksheet object to generate tasks to.
	 * @return Selected tasks.
	 */
	@Override
	public Iterable<TasksheetTask> generateTasks(final Tasksheet tasksheet) {
		log.info(String.format(
			GENERATING_TEMPLATE, tasksheet.getId(), tasksheet.getUsername()
		));
		
		java.util.List<Task> tasks = taskRepository
			.findByTopicId(tasksheet.getExam().getTopic().getId());
		java.util.Collections.shuffle(tasks);
		
		java.util.List<TasksheetTask> temp = new java.util.ArrayList<>();
		int max = Math.min(tasksheet.getExam().getTaskCount(), tasks.size());
		for (int i = 0; i < max; i++) {
			temp.add(tasksheetTaskRepository.save(
				TasksheetTask.builder()
					.tasksheet(tasksheet)
					.answered(false)
					.task(tasks.get(i))
					.build()
			));
		}
		
		return temp;
	}

}
