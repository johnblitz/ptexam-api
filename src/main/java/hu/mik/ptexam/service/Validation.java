package hu.mik.ptexam.service;

import java.time.LocalDateTime;

import hu.mik.ptexam.domain.*;
import hu.mik.ptexam.utils.IPV4;
import hu.mik.ptexam.utils.IPV4Utils;
import hu.mik.ptexam.utils.PrincipalUtils;

import static hu.mik.ptexam.utils.Simplification.*;

public final class Validation {

	private Validation() {
	}
	
	public static LocalDateTime isInTime(Exam exam, long additionalTime) {
		final LocalDateTime current = requireNonNull(LocalDateTime.now());
		final LocalDateTime start = requireNonNull(exam.getStartTime());
		final LocalDateTime end = requireNonNull(exam.getEndTime());
		if (!current.isAfter(start) || !current.isBefore(end.plusMinutes(additionalTime)))
			throw new IllegalStateException("Exam must be in the correct time interval");
		return current;
	}
	
	public static IPV4 isInValidIpRange(Classroom classroom) {
		IPV4 address = requireNonNull(IPV4Utils.getClientIP());
		IPV4 start = requireNonNull(classroom.getIpStart());
		IPV4 end = requireNonNull(classroom.getIpEnd());
		if (!address.isBetween(start, end))
			throw new IllegalStateException("IP address must be in range");
		return address;
	}
	
	public static String isUserValid() {
		String username = requireNonNull(PrincipalUtils.getName());
		if ("System".equals(username) || isNullOrWhitespace(username))
			throw new IllegalStateException("Non-System and Non-Empty User(name) required");
		return username;
	}
	
	public static boolean isTerminated(Tasksheet tasksheet, boolean requirement) {
		if (tasksheet.isTerminated() != requirement)
			throw new IllegalStateException(
					String.format(
						"Tasksheet requires termination=%s, but is terminated=%s",
						requirement,
						tasksheet.isTerminated()
					)
				);
		return tasksheet.isTerminated();
	}
	
	public static long remainingTime(Exam exam, LocalDateTime current) {
		return java.time.temporal.ChronoUnit.SECONDS.between(exam.getEndTime(), current);
	}
	
	public static boolean isCoding(TasksheetTask tasksheetTask) {
		return requireNonNull(tasksheetTask.getTask()).isCoding();
	}
	
	public static boolean isTextual(TasksheetTask tasksheetTask) {
		return requireNonNull(tasksheetTask.getTask()).isTextual();
	}
	
}
