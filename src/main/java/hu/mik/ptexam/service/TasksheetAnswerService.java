package hu.mik.ptexam.service;

import hu.mik.ptexam.domain.TasksheetAnswer;
import hu.mik.ptexam.domain.dtos.AnswerDto;

public interface TasksheetAnswerService {

	Iterable<TasksheetAnswer> submit(
		String examId, Long tasksheetTaskId, AnswerDto answer);
	
}
