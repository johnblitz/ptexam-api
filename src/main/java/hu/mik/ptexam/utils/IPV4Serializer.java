package hu.mik.ptexam.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public final class IPV4Serializer extends JsonSerializer<IPV4> {

	@Override
	public void serialize(IPV4 ip, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeString(ip.toString());
	}

}
