package hu.mik.ptexam.utils;

public final class Simplification {

	private static final String REQUIREMENT = "Requirement is NOT fullfilled";
	private static final String REQUIRE_NON_NULL = "Object must NOT be null";
	
	private Simplification() {
	}
	
	public static boolean require(boolean requirement) {
		if (!requirement) throw new IllegalStateException(REQUIREMENT);
		return requirement;
	}
	
	public static <T> T requireNonNull(T object) {
		if (object == null) throw new NullPointerException(REQUIRE_NON_NULL);
		return object;
	}

	public static <T> T requireNonNull(java.util.Optional<T> optional) {
		if (!optional.isPresent() || optional.get() == null)
			throw new NullPointerException(REQUIRE_NON_NULL);
		return optional.get();
	}
	
	public static <T> java.util.List<T> requireNonNull(java.util.List<T> list) {
		if (list == null || list.isEmpty())
			throw new NullPointerException(REQUIRE_NON_NULL);
		return list;
	}
	
	public static <T> java.util.Set<T> requireNonNull(java.util.Set<T> set) {
		if (set == null || set.isEmpty())
			throw new NullPointerException(REQUIRE_NON_NULL);
		return set;
	}
	
	public static String requireNonNull(String s) {
		if (isNullOrWhitespace(s))
			throw new NullPointerException(REQUIRE_NON_NULL);
		return s;
	}
	
	public static boolean isNullOrEmpty(String s) {
		return s == null || s.length() == 0;
	}

	public static boolean isNullOrWhitespace(String s) {
		return s == null || s.length() == 0 || isWhitespace(s);
	}
	
	protected static boolean isWhitespace(String s) {
		if (s == null) return false;
		int length = s.length();
		if (length > 0) {
			for (int start = 0, middle = length / 2, end = length - 1; start <= middle; start++, end--) {
				if (s.charAt(start) > ' ' || s.charAt(end) > ' ') return false;
			}
			return true;
		}
		return false;
	}
	
	public static <T> boolean equalsAny(T t, Iterable<T> array) {
		java.util.Iterator<T> iterator = array.iterator();
		while(iterator.hasNext()) {
			if (iterator.next().equals(t)) return true;
		}
		return false;
	}
	
}
