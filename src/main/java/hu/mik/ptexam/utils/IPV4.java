package hu.mik.ptexam.utils;

import java.io.Serializable;

import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Sample:
 * 192.168.1.2 = 11000000.10101000.00000001.00000010
 * binary value = 11000000 10101000 00000001 00000010
 * valueOf(192.168.1.2) = 3232235778L
 * 
 * @author Balla Dávid
 *
 */
@JsonSerialize(using = IPV4Serializer.class)
@JsonDeserialize(using = IPV4Deserializer.class)
public class IPV4 implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String BYTE_COUNT_ERROR = "Bytes array length must be 4";
	
	private static final String REGEX_PATTERN = "([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})";
	
	private static final long BYTE_MASK = 0x00000000000000ffL;
	
	private byte[] bytes = new byte[4];
	
	private long value;
	
	public IPV4() {
		this.value = valueOf(this.bytes);
	}
	
	public IPV4(byte[] bytes) {
		if (bytes.length != 4) throw new IllegalArgumentException(BYTE_COUNT_ERROR);
		this.bytes = bytes;
		this.value = valueOf(this.bytes);
	}
	
	public IPV4(byte first, byte second, byte third, byte fourth) {
		this.bytes = new byte[] { first, second, third, fourth };
		this.value = valueOf(this.bytes);
	}
	
	public IPV4(int first, int second, int third, int fourth) {
		this.bytes = new byte[] { (byte)first, (byte)second, (byte)third, (byte)fourth };
		this.value = valueOf(this.bytes);
	}
	
	public IPV4(long first, long second, long third, long fourth) {
		this.bytes = new byte[] { (byte)first, (byte)second, (byte)third, (byte)fourth };
		this.value = valueOf(this.bytes);
	}
	
	public static long valueOf(byte[] bytes) {
		if (bytes.length != 4) throw new IllegalArgumentException(BYTE_COUNT_ERROR);
		return valueOf(bytes[0] & BYTE_MASK, bytes[1] & BYTE_MASK, bytes[2] & BYTE_MASK, bytes[3] & BYTE_MASK);
	}
	
	public static long valueOf(byte first, byte second, byte third, byte fourth) {
		return valueOf(first & BYTE_MASK, second & BYTE_MASK, third & BYTE_MASK, fourth & BYTE_MASK);
	}
	
	public static long valueOf(int first, int second, int third, int fourth) {
		return valueOf(first & BYTE_MASK, second & BYTE_MASK, third & BYTE_MASK, fourth & BYTE_MASK);
	}
	
	public static long valueOf(long first, long second, long third, long fourth) {
		return (first << 24) + (second << 16) + (third << 8) + fourth;
	}
	
	public long value() {
		return value;
	}
	
	public boolean isBefore(IPV4 max) {
		return max.value() >= this.value();
	}
	
	public boolean isAfter(IPV4 min) {
		return min.value() <= this.value();
	}
	
	public boolean isBetween(IPV4 min, IPV4 max) {
		return isBefore(max) && isAfter(min);
	}

	@Override
	public String toString() {
		return String.format("%d.%d.%d.%d", bytes[0] & 0xFF, bytes[1] & 0xFF, bytes[2] & 0xFF, bytes[3] & 0xFF);
	}
	
	public static List<IPV4> parse(String source) {
		List<IPV4> addresses = new ArrayList<>();
		Pattern pattern = Pattern.compile(REGEX_PATTERN);
		Matcher matcher = pattern.matcher(source.trim());
		while (matcher.find()) {
			int a = Integer.parseInt(matcher.group(1));
			int b = Integer.parseInt(matcher.group(2));
			int c = Integer.parseInt(matcher.group(3));
			int d = Integer.parseInt(matcher.group(4));
			IPV4 ipv4 = new IPV4(a, b, c, d);
			addresses.add(ipv4);
		}
		return addresses;
	}
	
	public static IPV4 of(long value) {
		return new IPV4(value >>> 24, value >>> 16, value >>> 8, value);
	}
	
}
