package hu.mik.ptexam.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class PrincipalUtils {

	private PrincipalUtils() {
	}
	
	public static String getName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return auth == null ? "System" : auth.getName();
	}
	
}
