package hu.mik.ptexam.utils;

import javax.persistence.Converter;
import javax.persistence.AttributeConverter;

@Converter(autoApply = true)
public class IPV4Converter implements AttributeConverter<IPV4, Long> {

	@Override
	public Long convertToDatabaseColumn(IPV4 ip) {
		return ip.value();
	}

	@Override
	public IPV4 convertToEntityAttribute(Long dbData) {
		return IPV4.of(dbData);
	}

}
