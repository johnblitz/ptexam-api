package hu.mik.ptexam.utils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.DeserializationContext;

public class IPV4Deserializer extends JsonDeserializer<IPV4> {

	@Override
	public IPV4 deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		return IPV4.parse(node.asText()).stream().findFirst().orElse(new IPV4());
	}

}
