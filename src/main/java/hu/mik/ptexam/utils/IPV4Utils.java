package hu.mik.ptexam.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public final class IPV4Utils {

	private IPV4Utils() {
	}

	private static final String[] HEADERS = {
		"X-Forwarded-For",
		"Proxy-Client-IP",
		"WL-Proxy-Client-IP",
		"HTTP_X_FORWARDED_FOR",
		"HTTP_X_FORWARDED",
		"HTTP_X_CLUSTER_CLIENT_IP",
		"HTTP_CLIENT_IP",
		"HTTP_FORWARDED_FOR",
		"HTTP_FORWARDED",
		"HTTP_VIA",
		"REMOTE_ADDR"
	};
	
	public static IPV4 getClientIP() {
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
			for (String header : HEADERS) {
				String ips = request.getHeader(header);
				if (ips != null && ips.length() != 0 && !"unknown".equalsIgnoreCase(ips)) {
					String lastIP = ips.split(",")[0];
					return IPV4.parse(lastIP).stream().findFirst().orElse(IPV4.of(0L));
				}
			}
			String remote = request.getRemoteAddr();
			if (!"0:0:0:0:0:0:0:1".equalsIgnoreCase(remote)) {
				return IPV4.parse(remote).stream().findFirst().orElse(IPV4.of(0L));
			}
		}
		catch(NullPointerException npe) {
			return IPV4.of(0L);
		}
		return IPV4.of(0L);
	}
	
}
