package hu.mik.ptexam.dao;

import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hu.mik.ptexam.domain.*;

@RepositoryRestResource(path = Classroom.FLD_CLASSROOMS, exported = true)
public interface ClassroomRepository extends PagingAndSortingRepository<Classroom, Long> {

	Classroom findByLocation(@Param(Classroom.COL_LOCATION) String location);
	
}
