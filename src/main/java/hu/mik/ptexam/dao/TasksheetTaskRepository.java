package hu.mik.ptexam.dao;

import org.springframework.data.repository.CrudRepository;

import hu.mik.ptexam.domain.*;

public interface TasksheetTaskRepository extends CrudRepository<TasksheetTask, Long> {

}
