package hu.mik.ptexam.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hu.mik.ptexam.domain.*;

@RepositoryRestResource(path = Topic.FLD_TOPICS, exported = true)
public interface TopicRepository extends PagingAndSortingRepository<Topic, Long> {

	Topic findByName(@Param(Topic.COL_NAME) String name);
	
	List<Topic> findBySubjectId(@Param(Subject.COL_ID) Long id);
	
	List<Topic> findBySubjectCode(@Param(Subject.COL_CODE) String code);
	
	List<Topic> findBySubjectName(@Param(Subject.COL_NAME) String name);
	
}
