package hu.mik.ptexam.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hu.mik.ptexam.domain.*;

@RepositoryRestResource(path = Subject.FLD_SUBJECTS, exported = true)
public interface SubjectRepository extends PagingAndSortingRepository<Subject, Long> {

	Optional<Subject> findByCode(@Param(Subject.COL_CODE) String code);
	
	List<Subject> findByName(@Param(Subject.COL_NAME) String name);
	
}
