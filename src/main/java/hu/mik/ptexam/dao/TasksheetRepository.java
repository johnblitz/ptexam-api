package hu.mik.ptexam.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import hu.mik.ptexam.domain.Tasksheet;

public interface TasksheetRepository extends CrudRepository<Tasksheet, Long> {

	Optional<Tasksheet> findByExamIdAndUsername(String examId, String username);
	
}
