package hu.mik.ptexam.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import hu.mik.ptexam.domain.*;

public interface TasksheetAnswerRepository extends CrudRepository<TasksheetAnswer, Long> {

	List<TasksheetAnswer> findByTasksheetTaskIdAndUsername(Long id, String username);
	
}
