package hu.mik.ptexam.dao;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import hu.mik.ptexam.domain.Result;

@Repository
public interface ResultRepository extends CrudRepository<Result, Long> {

	Iterable<Result> findByUsername(String username);
	
	Result findByUsernameAndTasksheetId(String username, Long id);
	
}
