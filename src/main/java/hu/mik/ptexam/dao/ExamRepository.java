package hu.mik.ptexam.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hu.mik.ptexam.domain.*;

@RepositoryRestResource(path = Exam.FLD_EXAMS, exported = true)
public interface ExamRepository extends PagingAndSortingRepository<Exam, String> {

	List<Exam> findByTopicId(@Param(Topic.COL_ID) Long id);
	
	List<Exam> findByTopicName(@Param(Topic.COL_NAME) String name);
	
	List<Exam> findByClassroomId(@Param(Classroom.COL_ID) Long id);
	
	List<Exam> findByClassroomLocation(@Param(Classroom.COL_LOCATION) String location);
	
}
