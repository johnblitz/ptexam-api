package hu.mik.ptexam.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hu.mik.ptexam.domain.*;

@RepositoryRestResource(path = Solution.FLD_SOLUTIONS, exported = true)
public interface SolutionRepository extends PagingAndSortingRepository<Solution, String> {

	List<Solution> findByTaskId(@Param(Task.COL_ID) String id);
	
	List<Solution> findByTaskTopicId(@Param(Topic.COL_ID) Long id);
	
	List<Solution> findByTaskTopicName(@Param(Topic.COL_NAME) String name);
	
}
