package hu.mik.ptexam.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.PagingAndSortingRepository;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import hu.mik.ptexam.domain.*;

@RepositoryRestResource(path = Task.FLD_TASKS, exported = true)
public interface TaskRepository extends PagingAndSortingRepository<Task, String> {

	List<Task> findByTopicId(@Param(Topic.COL_ID) Long id);
}
