package hu.mik.ptexam.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SimplificationTest {
	
	private Object expected;
	private Object actual;
	
	@BeforeEach
	public void setup() {
		this.expected = null;
		this.actual = null;
	}
	
	@Test
	public void require_when_true_expect_noError() {
		assertEquals(true, Simplification.require(true));
	}
	
	@Test
	public void require_when_false_expect_IllegalStateException() {
		assertThrows(
			IllegalStateException.class,
			() -> assertEquals(false, Simplification.require(false))
		);
	}
	
	@Test
	public void requireNonNull_T_when_objectIsNotNull_expect_noError() {
		this.expected = new Object();
		this.actual = Simplification.requireNonNull(new Object());
		assertEquals(
			this.expected instanceof Object,
			this.actual instanceof Object,
			"Should be equal at this point"
		);
	}
	
	@Test
	public void requireNonNull_Optional_T_when_optionalIsNotNull_expect_noError() {
		this.expected = new Object();
		this.actual = Simplification.requireNonNull(java.util.Optional.of(new Object()));
		assertEquals(
			this.expected instanceof Object,
			this.actual instanceof Object,
			"Should be equal at this point"
		);
	}
	
	@Test
	public void requireNonNull_List_T_when_listIsNotNull_expect_noError() {
		this.expected = java.util.Arrays.asList(new Object());
		this.actual = Simplification.requireNonNull(java.util.Arrays.asList(new Object()));
		assertEquals(
			this.expected instanceof java.util.List,
			this.actual instanceof java.util.List,
			"Should be equal at this point"
		);
	}
	
	@Test
	public void requireNonNull_Set_T_when_setIsNotNull_expect_noError() {
		this.expected = java.util.Arrays.asList(new Object());
		this.actual = Simplification.requireNonNull(java.util.Arrays.asList(new Object()));
		assertEquals(
			this.expected instanceof java.util.Set,
			this.actual instanceof java.util.Set,
			"Should be equal at this point"
		);
	}
	
	@Test
	public void requireNonNull_String_when_sIsNotNull_expect_noError() {
		this.expected = "not null";
		this.actual = Simplification.requireNonNull("not null");
		assertEquals(this.expected instanceof String, this.actual instanceof String);
		assertEquals(this.expected, this.actual);
	}
	
	@Test
	public void requireNonNull_T_when_objectIsNull_expect_NullPointerException() {
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull(this.actual),
			"Should throw a NullPointerException"
		);
	}
	
	@Test
	public void requireNonNull_Optional_T_when_optionalIsNull_expect_NullPointerException() {
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull(java.util.Optional.of(null)),
			"Should throw a NullPointerException"
		);
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull(java.util.Optional.empty()),
			"Should throw a NullPointerException"
		);
	}

	@Test
	public void requireNonNull_List_T_when_listIsNull_expect_NullPointerException() {
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull(new java.util.ArrayList<>()),
			"Should throw a NullPointerException"
		);
	}
	
	@Test
	public void requireNonNull_Set_T_when_setIsNull_expect_NullPointerException() {
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull(new java.util.HashSet<>()),
			"Should throw a NullPointerException"
		);
	}
	
	@Test
	public void requireNonNull_String_when_sIsNull_expect_NullPointerException() {
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull(""),
			"Should throw a NullPointerException"
		);
		assertThrows(
			NullPointerException.class,
			() -> Simplification.requireNonNull((String) null),
			"Should throw a NullPointerException"
		);
	}
	
	@Test
	public void when_isNullOrEmptyCalled_expect_goodValues() {
		assertEquals(false, Simplification.isNullOrEmpty("1"));
		assertEquals(true, Simplification.isNullOrEmpty(""));
		assertEquals(true, Simplification.isNullOrEmpty(null));
	}
	
	@Test
	public void when_isWhitespaceCalled_expect_goodValues() {
		assertEquals(true, Simplification.isWhitespace(" "));
		assertEquals(true, Simplification.isWhitespace("  "));
		assertEquals(true, Simplification.isWhitespace("\n"));
		assertEquals(true, Simplification.isWhitespace("  \n"));
		assertEquals(true, Simplification.isWhitespace("\r"));
		assertEquals(false, Simplification.isWhitespace(""));
		assertEquals(false, Simplification.isWhitespace("1"));
		assertEquals(false, Simplification.isWhitespace(null));
	}
	
	@Test
	public void when_isNullOrWhitespaceCalled_expect_goodValues() {
		assertEquals(false, Simplification.isNullOrWhitespace("1"));
		assertEquals(false, Simplification.isNullOrWhitespace("1 "));
		assertEquals(false, Simplification.isNullOrWhitespace(" 1"));
		assertEquals(false, Simplification.isNullOrWhitespace(" 1\n"));
		assertEquals(false, Simplification.isNullOrWhitespace(" 1 \n"));
		assertEquals(true, Simplification.isNullOrWhitespace(""));
		assertEquals(true, Simplification.isNullOrWhitespace(" "));
		assertEquals(true, Simplification.isNullOrWhitespace("  "));
		assertEquals(true, Simplification.isNullOrWhitespace("\r\n"));
		assertEquals(true, Simplification.isNullOrWhitespace("\r \r"));
		assertEquals(true, Simplification.isNullOrWhitespace(null));
	}
	
	@Test
	public void equalsAnyTest() {
		assertEquals(true, Simplification.equalsAny("a", java.util.Arrays.asList("a", "b", "c")));
		assertEquals(false, Simplification.equalsAny("d", java.util.Arrays.asList("a", "b", "c")));
	}
	
}
