package hu.mik.ptexam.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class IPV4Test {

	@Test
	void When_valueOfIsFullZero_Expect_valueOfEqualsZero() {
		long expected = 0L;
		assertEquals(expected, IPV4.valueOf(0L, 0L, 0L, 0L));
		assertEquals(expected, IPV4.valueOf(0, 0, 0, 0));
		assertEquals(expected, IPV4.valueOf((byte)0, (byte)0, (byte)0, (byte)0));
		assertEquals(expected, IPV4.valueOf(new byte[4]));
	}
	
	@Test
	void When_valueOfBytesArrayLengthNot4_Expect_throwIllegalArgumentException() {
		assertThrows(IllegalArgumentException.class, () -> IPV4.valueOf(new byte[3]));
		assertThrows(IllegalArgumentException.class, () -> IPV4.valueOf(new byte[5]));
	}

}
