package hu.mik.ptexam;

import org.junit.jupiter.api.*;
import org.springframework.restdocs.*;
import org.springframework.http.MediaType;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.security.test.context.support.WithMockUser;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;

import hu.mik.ptexam.Application;

@ExtendWith(value = {
	RestDocumentationExtension.class,
	SpringExtension.class
})
@SpringBootTest(
	classes = { Application.class },
	webEnvironment = WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
@AutoConfigureRestDocs
public class ApiTest {

	private MockMvc mvc;
	
	@Autowired
	private WebApplicationContext context;

	@BeforeEach
	public void setup(RestDocumentationContextProvider restDocumentation) {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context)
			.apply(documentationConfiguration(restDocumentation))
			.apply(springSecurity())
			.build();
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER", "STUDENT" })
	public void when_accessingIndex_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		this.mvc.perform(get("/"))
			.andExpect(status().isOk())
			.andDo(document("index",
				links(
					linkWithRel("api").description("Link to the api"),
					linkWithRel("self").description("Link to the index"),
					linkWithRel("users").description("Link to the users")
				)
			));
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN" })
	public void when_accessingUsers_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		this.mvc.perform(get("/users")).andExpect(status().isOk()).andDo(
			document("users", links(
				linkWithRel("register")
					.description("Link to the user registration"),
				linkWithRel("self").description("Link to the users"),
				linkWithRel("index").description("Link to the index")
			)));
		try {
		this.mvc.perform(post("/users/register")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"user_name\":\"...\", \"user_psswrd\":\"...\", "
				+ "\"role_name\":\"ROLE_STUDENT\" }"))
			.andExpect(status().isOk())
			.andDo(document("users/register"));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@WithMockUser(roles = { "TEACHER", "STUDENT" })
	public void when_accessingUsers_then_shouldBeForbidden() throws Exception {
		this.mvc.perform(get("/users")).andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER", "STUDENT" })
	public void when_accessingApi_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		this.mvc.perform(get("/api"))
			.andExpect(status().isOk())
			.andDo(document("api",
				links(
					linkWithRel("v1").description("Link to the v1 functions"),
					linkWithRel("self").description("Link to the api"),
					linkWithRel("index").description("Link to the index")
				)
			));
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER", "STUDENT" })
	public void when_accessingV1_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		this.mvc.perform(get("/api/v1"))
			.andExpect(status().isOk())
			.andDo(document("api/v1", links(
				linkWithRel("solutions").description("Link to the solutions"),
				linkWithRel("subjects").description("Link to the subjects"),
				linkWithRel("classrooms").description("Link to the classrooms"),
				linkWithRel("exams").description("Link to the exams"),
				linkWithRel("tasks").description("Link to the tasks"),
				linkWithRel("topics").description("Link to the topics"),
				linkWithRel("tasksheets").description("Link to the tasksheets"),
				linkWithRel("results").description("Link to the results"),
				linkWithRel("self").description("Link to the v1"),
				linkWithRel("api").description("Link to the api"),
				linkWithRel("index").description("Link to the index"),
				linkWithRel("profile").description("Link to the profiles")
			)));
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER" })
	public void when_accessingSubjects_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		/* CRUD: READ */
		this.mvc.perform(get("/api/v1/subjects"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/subjects/crud/read",
				links(
					linkWithRel("self").description("Link to the subjects"),
					linkWithRel("profile").description("Link to the profile"),
					linkWithRel("search").description("Link to the search")
				)
			));
		this.mvc.perform(get("/api/v1/subjects/1003"))
			.andExpect(status().isNotFound());
		/* CRUD: CREATE */
		this.mvc.perform(post("/api/v1/subjects")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"subject_code\":\"...\", \"subject_name\":\"...\" }"))
			.andExpect(status().isCreated())
			.andDo(document("api/v1/subjects/crud/create"));
		this.mvc.perform(get("/api/v1/subjects/1003"))
			.andExpect(status().isOk());
		/* CRUD: UPDATE */
		this.mvc.perform(put("/api/v1/subjects/1003")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"subject_code\":\"...\", \"subject_name\":\"!!!\" }"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/subjects/crud/update"));
		/* CRUD: DELETE */
		this.mvc.perform(delete("/api/v1/subjects/1003"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/subjects/crud/delete"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/subjects/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/subjects/search"));
		/* FIND_BY_NAME */
		this.mvc.perform(get("/api/v1/subjects/search/findByName")
			.param("subject_name", "Szakdolgozat"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/subjects/search/findByName",
				requestParameters(parameterWithName("subject_name")
					.description("The subject's name"))
			));
		/* FIND_BY_CODE */
		this.mvc.perform(get("/api/v1/subjects/search/findByCode")
			.param("subject_code", "PMTMINB238H"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/subjects/search/findByCode",
				requestParameters(parameterWithName("subject_code")
					.description("The subject's code"))
			));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingSubjects_then_shouldBeForbidden()
			throws Exception {
		this.mvc.perform(get("/api/v1/subjects"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER" })
	public void when_accessingTopics_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		/* CRUD: READ */
		this.mvc.perform(get("/api/v1/topics"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/topics/crud/read",
				links(
					linkWithRel("self").description("Link to the topics"),
					linkWithRel("profile").description("Link to the profile"),
					linkWithRel("search").description("Link to the search")
				)
			));
		this.mvc.perform(get("/api/v1/topics/1003"))
			.andExpect(status().isNotFound());
		/* CRUD: CREATE */
		this.mvc.perform(post("/api/v1/topics")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"topic_name\":\"...\", \"subject\":"
				+ "\"http://localhost:8080/api/v1/subjects/1000\" }"))
			.andExpect(status().isCreated())
			.andDo(document("api/v1/topics/crud/create"));
		this.mvc.perform(get("/api/v1/topics/1003")).andExpect(status().isOk());
		/* CRUD: UPDATE */
		this.mvc.perform(put("/api/v1/topics/1003")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"topic_name\":\"!!!\", \"subject\":"
				+ "\"http://localhost:8080/api/v1/subjects/1000\" }"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/topics/crud/update"));
		/* CRUD: DELETE */
		this.mvc.perform(delete("/api/v1/topics/1003"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/topics/crud/delete"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/topics/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/topics/search"));
		/* FIND_BY_SUBJECT_CODE */
		this.mvc.perform(get("/api/v1/topics/search/findBySubjectCode")
			.param("subject_code", "PMTMINB238H"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/topics/search/findBySubjectCode",
				requestParameters(parameterWithName("subject_code")
					.description("The subject's code"))
			));
		/* FIND_BY_SUBJECT_NAME */
		this.mvc.perform(get("/api/v1/topics/search/findBySubjectName")
			.param("subject_name", "Szakdolgozat"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/topics/search/findBySubjectName",
				requestParameters(parameterWithName("subject_name")
					.description("The subject's name"))
			));
		/* FIND_BY_SUBJECT_ID */
		this.mvc.perform(get("/api/v1/topics/search/findBySubjectId")
			.param("id", "1000"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/topics/search/findBySubjectId",
				requestParameters(parameterWithName("id")
					.description("The subject's id"))
			));
		/* FIND_BY_NAME */
		this.mvc.perform(get("/api/v1/topics/search/findByName")
			.param("topic_name", "szakdolgozat_default"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/topics/search/findByName",
				requestParameters(parameterWithName("topic_name")
					.description("The topic's name"))
			));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingTopics_then_shouldBeForbidden() throws Exception {
		this.mvc.perform(get("/api/v1/topics"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER" })
	public void when_accessingTasks_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		/* CRUD: READ */
		this.mvc.perform(get("/api/v1/tasks"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasks/crud/read",
				links(
					linkWithRel("self").description("Link to the tasks"),
					linkWithRel("profile").description("Link to the profile"),
					linkWithRel("search").description("Link to the search")
				)
			));
		/* CRUD: CREATE */
		MvcResult result = this.mvc.perform(post("/api/v1/tasks")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"task_question\":\"...\", \"task_is_coding\":false, "
				+ "\"task_is_textual\":false, \"topic\":"
				+ "\"http://localhost:8080/api/v1/topics/1000\" }"))
			.andExpect(status().isCreated())
			.andDo(document("api/v1/tasks/crud/create"))
			.andReturn();
		String[] temp = result.getResponse().getHeader("Location").split("/");
		String id = temp[temp.length - 1];
		assertNotNull(id);
		/* CRUD: UPDATE */
		this.mvc.perform(put(String.format("/api/v1/tasks/%s", id))
			.contentType(MediaType.APPLICATION_JSON)
			.content(String.format("{ \"task_question\":\"%s\", "
				+ "\"task_is_coding\":\"%s\", \"task_is_textual\":\"%s\", "
				+ "\"topic\":\"%s\" }", "!!!", true, false,
				"http://localhost:8080/api/v1/topics/1000")))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/tasks/crud/update"));
		/* CRUD: DELETE */
		this.mvc.perform(delete(String.format("/api/v1/tasks/%s", id)))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/tasks/crud/delete"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/tasks/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasks/search"));
		/* FIND_BY_TOPIC_ID */
		this.mvc.perform(get("/api/v1/tasks/search/findByTopicId")
			.param("id", "1000"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasks/search/findByTopicId",
				requestParameters(parameterWithName("id")
					.description("The topic's id"))
			));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingTasks_then_shouldBeForbidden() throws Exception {
		this.mvc.perform(get("/api/v1/tasks"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER" })
	public void when_accessingSolutions_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		/* CRUD: READ */
		this.mvc.perform(get("/api/v1/solutions"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/solutions/crud/read",
				links(
					linkWithRel("first").description("Link to the first"),
					linkWithRel("self").description("Link to the solutions"),
					linkWithRel("next").description("Link to the next"),
					linkWithRel("last").description("Link to the last"),
					linkWithRel("profile").description("Link to the profile"),
					linkWithRel("search").description("Link to the search")
				)
			));
		/* CRUD: CREATE */
		MvcResult result1 = this.mvc.perform(post("/api/v1/tasks")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"task_question\":\"...\", \"task_is_coding\":false, "
				+ "\"task_is_textual\":false, \"topic\":"
				+ "\"http://localhost:8080/api/v1/topics/1000\" }"))
			.andExpect(status().isCreated())
			.andReturn();
		String[] temp1 = result1.getResponse().getHeader("Location").split("/");
		String id1 = temp1[temp1.length - 1];
		assertNotNull(id1);
		MvcResult result2 = this.mvc.perform(post("/api/v1/solutions")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"solution_answer\":\"...\", \"solution_is_correct\":"
				+ "false, \"task\":"
				+ "\"http://localhost:8080/api/v1/tasks/" + id1 + "\" }"))
			.andExpect(status().isCreated())
			.andDo(document("api/v1/solutions/crud/create"))
			.andReturn();
		String[] temp2 = result2.getResponse().getHeader("Location").split("/");
		String id2 = temp2[temp2.length - 1];
		assertNotNull(id2);
		/* CRUD: UPDATE */
		this.mvc.perform(put(String.format("/api/v1/solutions/%s", id2))
			.contentType(MediaType.APPLICATION_JSON)
			.content(String.format("{ \"solution_answer\":\"%s\", "
				+ "\"solution_is_correct\":%s, \"task\":\"%s\" }", "!!!", true,
				"http://localhost:8080/api/v1/tasks/" + id1)))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/solutions/crud/update"));
		/* CRUD: DELETE */
		this.mvc.perform(delete(String.format("/api/v1/solutions/%s", id2)))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/solutions/crud/delete"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/solutions/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/solutions/search"));
		/* FIND_BY_TASK_ID */
		this.mvc.perform(post("/api/v1/solutions")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"solution_answer\":\"...\", \"solution_is_correct\":"
				+ "false, \"task\":"
				+ "\"http://localhost:8080/api/v1/tasks/" + id1 + "\" }"))
			.andExpect(status().isCreated());
		this.mvc.perform(get("/api/v1/solutions/search/findByTaskId")
			.param("id", id1))
			.andExpect(status().isOk())
			.andDo(document("api/v1/solutions/search/findByTaskId",
				requestParameters(parameterWithName("id")
					.description("The task's id"))
			));
		/* FIND_BY_TASK_TOPIC_NAME */
		this.mvc.perform(get("/api/v1/solutions/search/findByTaskTopicName")
			.param("topic_name", "szakdolgozat_default"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/solutions/search/findByTaskTopicName",
				requestParameters(parameterWithName("topic_name")
					.description("The task's topic's name"))
			));
		/* FIND_BY_TASK_TOPIC_ID */
		this.mvc.perform(get("/api/v1/solutions/search/findByTaskTopicId")
			.param("id", "1000"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/solutions/search/findByTaskTopicId",
				requestParameters(parameterWithName("id")
					.description("The task's topic's id"))
			));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingSolutions_then_shouldBeForbidden()
			throws Exception {
		this.mvc.perform(get("/api/v1/solutions"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER" })
	public void when_accessingClassrooms_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		/* CRUD: READ */
		this.mvc.perform(get("/api/v1/classrooms"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/classrooms/crud/read",
				links(
					linkWithRel("self").description("Link to the classrooms"),
					linkWithRel("profile").description("Link to the profile"),
					linkWithRel("search").description("Link to the search")
				)
			));
		this.mvc.perform(get("/api/v1/classrooms/1006"))
			.andExpect(status().isNotFound());
		/* CRUD: CREATE */
		this.mvc.perform(post("/api/v1/classrooms")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"classroom_location\":\"...\", \"classroom_ip_start\":"
				+ "\"0.0.0.0\", \"classroom_ip_end\":\"255.255.255.255\" }"))
			.andExpect(status().isCreated())
			.andDo(document("api/v1/classrooms/crud/create"));
		this.mvc.perform(get("/api/v1/classrooms/1006"))
			.andExpect(status().isOk());
		/* CRUD: UPDATE */
		this.mvc.perform(put("/api/v1/classrooms/1006")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"classroom_location\":\"!!!\", \"classroom_ip_start\":"
				+ "\"127.0.0.1\", \"classroom_ip_end\":\"127.0.0.1\" }"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/classrooms/crud/update"));
		/* CRUD: DELETE */
		this.mvc.perform(delete("/api/v1/classrooms/1006"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/classrooms/crud/delete"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/classrooms/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/classrooms/search"));
		/* FIND_BY_LOCATION */
		this.mvc.perform(get("/api/v1/classrooms/search/findByLocation")
			.param("classroom_location", "A216"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/classrooms/search/findByLocation",
				requestParameters(parameterWithName("classroom_location")
					.description("The classroom's location"))
			));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingClassrooms_then_shouldBeForbidden()
			throws Exception {
		this.mvc.perform(get("/api/v1/classrooms"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER" })
	public void when_accessingExams_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		/* CRUD: READ */
		this.mvc.perform(get("/api/v1/exams"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/exams/crud/read",
				links(
					linkWithRel("self").description("Link to the exams"),
					linkWithRel("profile").description("Link to the profile"),
					linkWithRel("search").description("Link to the search")
				)
			));
		/* CRUD: CREATE */
		MvcResult result = this.mvc.perform(post("/api/v1/exams")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"exam_description\":\"...\", \"exam_start_time\":"
				+ "\"2019-05-01 00:00:00\", \"exam_end_time\":"
				+ "\"2019-05-30 23:59:59\", \"classroom\":"
				+ "\"http://localhost:8080/api/v1/classrooms/1000\", \"topic\":"
				+ "\"http://localhost:8080/api/v1/topics/1000\", "
				+ "\"exam_task_count\":3 }"))
			.andExpect(status().isCreated())
			.andDo(document("api/v1/exams/crud/create"))
			.andReturn();
		String[] temp = result.getResponse().getHeader("Location").split("/");
		String id = temp[temp.length - 1];
		assertNotNull(id);
		/* CRUD: UPDATE */
		this.mvc.perform(put(String.format("/api/v1/exams/%s", id))
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"exam_description\":\"!!!\", \"exam_start_time\":"
				+ "\"2019-05-01 00:00:00\", \"exam_end_time\":"
				+ "\"2019-05-30 23:59:59\", \"classroom\":"
				+ "\"http://localhost:8080/api/v1/classrooms/1000\", \"topic\":"
				+ "\"http://localhost:8080/api/v1/topics/1000\", "
				+ "\"exam_task_count\":3 }"))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/exams/crud/update"));
		/* CRUD: DELETE */
		this.mvc.perform(delete(String.format("/api/v1/exams/%s", id)))
			.andExpect(status().isNoContent())
			.andDo(document("api/v1/exams/crud/delete"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/exams/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/exams/search"));
		/* FIND_BY_TOPIC_ID */
		this.mvc.perform(get("/api/v1/exams/search/findByTopicId")
			.param("id", "1000"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/exams/search/findByTopicId",
				requestParameters(parameterWithName("id")
					.description("The topic's id"))
			));
		/* FIND_BY_CLASSROOM_LOCATION */
		this.mvc.perform(get("/api/v1/exams/search/findByClassroomLocation")
			.param("classroom_location", "A202"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/exams/search/findByClassroomLocation",
				requestParameters(parameterWithName("classroom_location")
					.description("The classroom's location"))
			));
		/* FIND_BY_CLASSROOM_ID */
		this.mvc.perform(get("/api/v1/exams/search/findByClassroomId")
			.param("id", "1002"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/exams/search/findByClassroomId",
				requestParameters(parameterWithName("id")
					.description("The classroom's id"))
			));
		/* FIND_BY_TOPIC_NAME */
		this.mvc.perform(get("/api/v1/exams/search/findByTopicName")
			.param("topic_name", "szakdolgozat_default"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/exams/search/findByTopicName",
				requestParameters(parameterWithName("topic_name")
					.description("The topic's name"))
			));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingExams_then_shouldBeForbidden() throws Exception {
		this.mvc.perform(get("/api/v1/exams"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "TEACHER", "STUDENT" })
	public void when_accessingTasksheets_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		this.mvc.perform(get("/api/v1/tasksheets")).andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets", links(
				linkWithRel("self").description("Link to the tasksheets"),
				linkWithRel("tasksheet").description("Link to a tasksheet"),
				linkWithRel("terminate").description("Link to the termination"),
				linkWithRel("submit").description("Link to the answer submit"),
				linkWithRel("search").description("Link to the search")
			)));
	}
	
	@Test
	@WithMockUser(roles = { "TEACHER" })
	public void when_accessingTasksheets_then_shouldBeForbidden()
			throws Exception {
		this.mvc.perform(get("/api/v1/tasksheets/examIdTest"))
			.andExpect(status().isForbidden());
		this.mvc.perform(get("/api/v1/tasksheets/search"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN", "STUDENT" })
	public void when_usingTasksheets_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		MvcResult result = this.mvc.perform(post("/api/v1/exams")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"exam_description\":\"...\", \"exam_start_time\":"
				+ "\"2018-05-01 00:00:00\", \"exam_end_time\":"
				+ "\"2020-05-30 23:59:59\", \"classroom\":"
				+ "\"http://localhost:8080/api/v1/classrooms/1000\", \"topic\":"
				+ "\"http://localhost:8080/api/v1/topics/1000\", "
				+ "\"exam_task_count\":3 }"))
			.andExpect(status().isCreated())
			.andReturn();
		String[] temp = result.getResponse().getHeader("Location").split("/");
		String examId = temp[temp.length - 1];
		assertNotNull(examId);
		/* TASKSHEET GENERATION */
		this.mvc.perform(get(String.format("/api/v1/tasksheets/%s", examId)))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/generate"));
		/* TASKSHEET ANSWER SUBMIT */
		this.mvc.perform(post(String.format("/api/v1/tasksheets/%s/submit/1000",
			examId))
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"tasksheet_answer_comment\":\"...\", "
				+ "\"solutions\":[\"...\", \"...\"] }"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/submit"));
		/* TASKSHEET TERMINATION */
		this.mvc.perform(get(String.format("/api/v1/tasksheets/%s/terminate",
			examId)))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/terminate"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/tasksheets/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/search", links(
				linkWithRel("findAllAvailable")
					.description("Link to findAllAvailable"),
				linkWithRel("findAll").description("Link to findAll"),
				linkWithRel("self").description("Link to search")
			)));
		/* FIND_ALL_AVAILABLE */
		this.mvc.perform(get("/api/v1/tasksheets/search/findAllAvailable"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/search/findAllAvailable"));
		/* FIND_ALL */
		this.mvc.perform(get("/api/v1/tasksheets/search/findAll"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/search/findAll"));
	}
	
	@Test
	@WithMockUser(roles = { "STUDENT" })
	public void when_accessingResults_then_shouldBeForbidden()
			throws Exception {
		this.mvc.perform(get("/api/v1/results/search/findAll"))
			.andExpect(status().isForbidden());
		this.mvc.perform(get("/api/v1/results/search/findByUsername"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	@WithMockUser(roles = { "ADMIN" })
	public void when_accessingResults_then_shouldReturnOkStatusWithHalLinks()
			throws Exception {
		MvcResult result = this.mvc.perform(post("/api/v1/exams")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"exam_description\":\"...\", \"exam_start_time\":"
				+ "\"2018-05-01 00:00:00\", \"exam_end_time\":"
				+ "\"2020-05-30 23:59:59\", \"classroom\":"
				+ "\"http://localhost:8080/api/v1/classrooms/1000\", \"topic\":"
				+ "\"http://localhost:8080/api/v1/topics/1000\", "
				+ "\"exam_task_count\":3 }"))
			.andExpect(status().isCreated())
			.andReturn();
		String[] temp = result.getResponse().getHeader("Location").split("/");
		String examId = temp[temp.length - 1];
		assertNotNull(examId);
		/* TASKSHEET GENERATION */
		this.mvc.perform(get(String.format("/api/v1/tasksheets/%s", examId)))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/generate"));
		/* TASKSHEET ANSWER SUBMIT */
		this.mvc.perform(post(String.format("/api/v1/tasksheets/%s/submit/1000",
			examId))
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"tasksheet_answer_comment\":\"...\", "
				+ "\"solutions\":[\"...\", \"...\"] }"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/submit"));
		/* TASKSHEET TERMINATION */
		this.mvc.perform(get(String.format("/api/v1/tasksheets/%s/terminate",
			examId)))
			.andExpect(status().isOk())
			.andDo(document("api/v1/tasksheets/terminate"));
		/* RESULTS */
		this.mvc.perform(get("/api/v1/results")).andExpect(status().isOk())
			.andDo(document("api/v1/results", links(
				linkWithRel("result")
					.description("Link to the teacher result verification"),
				linkWithRel("self").description("Link to a results"),
				linkWithRel("search").description("Link to the search")
			)));
		/* VERIFY RESULT */
		this.mvc.perform(post("/api/v1/results/1000")
			.contentType(MediaType.APPLICATION_JSON)
			.content("{ \"result_grade\":\"5\", \"result_points\":\"30\" }"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/results/verify"));
		/* SEARCH */
		this.mvc.perform(get("/api/v1/results/search"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/results/search", links(
				linkWithRel("findAll").description("Link to findAll"),
				linkWithRel("findByUsername")
					.description("Link to findByUsername"),
				linkWithRel("findMyResults")
					.description("Link to findMyResults"),
				linkWithRel("self").description("Link to the search")
			)));
		/* FIND_MY_RESULTS */
		this.mvc.perform(get("/api/v1/results/search/findMyResults"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/results/search/findMyResults"));
		/* FIND_ALL */
		this.mvc.perform(get("/api/v1/results/search/findAll"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/results/search/findAll"));
		/* FIND_BY_USERNAME */
		this.mvc.perform(get("/api/v1/results/search/findByUsername")
			.param("username", "user"))
			.andExpect(status().isOk())
			.andDo(document("api/v1/results/search/findByUsername",
				requestParameters(parameterWithName("username")
					.description("The user's name"))));
	}
	
}
